import com.intellij.database.model.DasTable
import com.intellij.database.util.Case
import com.intellij.database.util.DasUtil

/*
 * Available context bindings:
 *   SELECTION   Iterable<DasObject>
 *   PROJECT     project
 *   FILES       files helper
 */

entityPackagePath = "\\entity"
repositoryPackagePath = "\\repository"
typeMapping = [
        (~/(?i)bigint/)                 : "long",
        (~/(?i)int/)                    : "int",
        (~/(?i)float|double|real/)      : "double",
        (~/(?i)decimal/)                : "java.math.BigDecimal",
        (~/(?i)date|datetime|timestamp/): "Date",
        (~/(?i)time/)                   : "java.sql.Time",
        (~/(?i)/)                       : "String"
]
typeStateMapping = [
        (~/(?i)bigint/)                 : "NumberPath<Long>",
        (~/(?i)int/)                    : "NumberPath<Integer>",
        (~/(?i)float|double|real/)      : "NumberPath<Double>",
        (~/(?i)decimal/)                : "NumberPath<java.math.BigDecimal>",
        (~/(?i)date|datetime|timestamp/): "DateTimePath<Date>",
        (~/(?i)time/)                   : "DateTimePath<java.sql.Time>",
        (~/(?i)/)                       : "StringPath"
]
javaTypesMapping = [
        (~/(?i)bigint/)                 : "BIGINT",
        (~/(?i)int/)                    : "INTEGER",
        (~/(?i)float|double|real/)      : "DOUBLE",
        (~/(?i)decimal/)                : "DECIMAL",
        (~/(?i)date|datetime|timestamp/): "TIMESTAMP",
        (~/(?i)time/)                   : "TIME",
        (~/(?i)/)                       : "VARCHAR"
]
entityPackageName = null
repositoryPackageName = null
FILES.chooseDirectoryAndSave("Choose directory", "Choose where to store generated files") { dir ->
    SELECTION.filter { it instanceof DasTable }.each { generate(it, dir) }
}

def generate(table, dir) {
    def className = javaName(table.getName(), true)
    def entityDir = new File(dir.path + entityPackagePath)
    if (!entityDir.exists())
        entityDir.mkdir()
    entityPackageName = entityDir.path.substring(entityDir.path.lastIndexOf("java") + 5, entityDir.path.length()).replace("\\", ".") + ";"
    def fields = calcFields(table)
    new File(entityDir, className + ".java").withPrintWriter { out -> generate(out, className, fields) }
    new File(entityDir, "Q$className" + ".java").withPrintWriter { out -> generateQ(out, className, table) }
    def repositoryDir = new File(dir.path + repositoryPackagePath)
    if (!repositoryDir.exists())
        repositoryDir.mkdir()
    repositoryPackageName = repositoryDir.path.substring(repositoryDir.path.lastIndexOf("java") + 5, repositoryDir.path.length()).replace("\\", ".") + ";"
    def repositoryFile = new File(repositoryDir, className + "Repository.java")
    if (!repositoryFile.exists())
        repositoryFile.withPrintWriter { out -> generateRepository(out, className) }
}


def generate(out, className, fields) {
    out.println "package $entityPackageName"
    out.println ""
    out.println "import org.hibernate.annotations.DynamicInsert;"
    out.println "import org.hibernate.annotations.DynamicUpdate;"
    out.println "import javax.persistence.Column;"
    out.println "import javax.persistence.Entity;"
    out.println "import javax.persistence.GeneratedValue;"
    out.println "import javax.persistence.Id;"
    out.println "import java.util.Date;"
    out.println ""
    out.println "@Entity"
    out.println "@DynamicInsert"
    out.println "@DynamicUpdate"
    out.println "public class $className {"
    out.println ""
    fields.each() {
        if (it.name == "id") {
            out.println "    @Id"
            out.println "    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)"
        }
        out.println "    @Column"
        out.println "    private ${it.type} ${it.name};"
        out.println ""
    }
    out.println ""
    fields.each() {
        out.println "    public ${it.type} get${it.name.capitalize()}() {"
        out.println "        return ${it.name};"
        out.println "    }"
        out.println ""
        out.println "    public void set${it.name.capitalize()}(${it.type} ${it.name}) {"
        out.println "        this.${it.name} = ${it.name};"
        out.println "    }"
        out.println ""
    }
    out.println "}"
}

def generateQ(out, className, table) {
    out.println "package $entityPackageName"
    out.println ""
    out.println "import static com.querydsl.core.types.PathMetadataFactory.*;"
    out.println "import com.querydsl.core.types.dsl.*;"
    out.println "import javax.annotation.Generated;"
    out.println "import com.querydsl.sql.ColumnMetadata;"
    out.println "import java.sql.Types;"
    out.println "import java.util.Date;"
    out.println ""
    out.println "/**"
    out.println " * Created By Sugar"
    out.println " */"
    out.println "@Generated(\"com.querydsl.sql.codegen.MetaDataSerializer\")"
    out.println "public class Q$className extends com.querydsl.sql.RelationalPathBase<$className> {"
    out.println ""
    out.println "    private static final long serialVersionUID = ${new Random().nextLong()}L;"
    out.println ""
    out.println "    public static final Q$className q$className = new Q$className(\"${table.getName()}\");"
    calcFieldsQ(out, table)
    out.println "\n    public final com.querydsl.sql.PrimaryKey<$className> primary = createPrimaryKey(id);"
    out.println ""
    out.println "    public Q$className(String variable) {"
    out.println "        super(${className}.class, forVariable(variable), \"null\", \"${table.getName()}\");"
    out.println "        addMetadata();"
    out.println "    }"
    out.println "\n    public void addMetadata() {"
    calcFieldsQF(out, table)
    out.println "    }"
    out.println "}"
}

def generateRepository(out, className) {
    out.println "package $repositoryPackageName"
    out.println ""
    out.println "import org.springframework.data.querydsl.QuerydslPredicateExecutor;"
    out.println "import ${entityPackageName.replace(";", ".$className")};"
    out.println "import org.springframework.data.jpa.repository.JpaRepository;"
    out.println ""
    out.println "public interface ${className}Repository extends JpaRepository<${className}, Long>,"
    out.println "        QuerydslPredicateExecutor<${className}> {"
    out.println "}"
}

def calcFields(table) {
    DasUtil.getColumns(table).reduce([]) { fields, col ->
        def spec = Case.LOWER.apply(col.getDataType().getSpecification())
        def typeStr = typeMapping.find { p, t -> p.matcher(spec).find() }.value
        fields += [[
                           name : javaName(col.getName(), false),
                           type : typeStr,
                           annos: ""]]
    }
}

def calcFieldsQ(out, table) {
    DasUtil.getColumns(table).each() {
        def spec = Case.LOWER.apply(it.getDataType().getSpecification())
        def typeState = typeStateMapping.find { p, t -> p.matcher(spec).find() }.value
        def createFun = "create" + typeState.replaceAll("Path.*", "")
        def field = javaName(it.getName(), false)
        def fieldClazz = ""
        if (!"StringPath".equals(typeState))
            fieldClazz = ", " + typeState.substring(typeState.lastIndexOf("<") + 1, typeState.lastIndexOf(">")) + ".class"
        out.println "\n    public final $typeState $field = $createFun(\"$field\"$fieldClazz);"
    }
}

def calcFieldsQF(out, table) {
    def count = 1
    DasUtil.getColumns(table).each() {
        def spec = Case.LOWER.apply(it.getDataType().getSpecification())
        def uSpec = javaTypesMapping.find { p, t -> p.matcher(spec).find() }.value
        def typeState = typeStateMapping.find { p, t -> p.matcher(spec).find() }.value
        def createFun = "create" + typeState.replaceAll("Path.*", "")
        def field = javaName(it.getName(), false)
        def fieldClazz = ""
        if (!"StringPath".equals(typeState))
            fieldClazz = ", " + typeState.substring(typeState.lastIndexOf("<") + 1, typeState.lastIndexOf(">")) + ".class"
        out.println "        addMetadata($field, ColumnMetadata.named(\"${it.getName()}\").withIndex($count).ofType(Types.$uSpec));"
        count++
    }
}

def javaName(str, capitalize) {
    def s = com.intellij.psi.codeStyle.NameUtil.splitNameIntoWords(str)
            .collect { Case.LOWER.apply(it).capitalize() }
            .join("")
            .replaceAll(/[^\p{javaJavaIdentifierPart}[_]]/, "_")
    capitalize || s.length() == 1 ? s : Case.LOWER.apply(s[0]) + s[1..-1]
}
