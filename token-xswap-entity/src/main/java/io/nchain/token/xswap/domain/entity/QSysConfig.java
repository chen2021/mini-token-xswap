package io.nchain.token.xswap.domain.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;
import com.querydsl.core.types.dsl.*;
import javax.annotation.Generated;
import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;
import java.util.Date;

/**
 * Created By Sugar
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QSysConfig extends com.querydsl.sql.RelationalPathBase<SysConfig> {

    private static final long serialVersionUID = -8301966601621931893L;

    public static final QSysConfig qSysConfig = new QSysConfig("sys_config");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath module = createString("module");

    public final StringPath name = createString("name");

    public final StringPath value = createString("value");

    public final StringPath description = createString("description");

    public final NumberPath<Long> creatorUid = createNumber("creatorUid", Long.class);

    public final NumberPath<Long> opUid = createNumber("opUid", Long.class);

    public final DateTimePath<Date> createdAt = createDateTime("createdAt", Date.class);

    public final DateTimePath<Date> updatedAt = createDateTime("updatedAt", Date.class);

    public final com.querydsl.sql.PrimaryKey<SysConfig> primary = createPrimaryKey(id);

    public QSysConfig(String variable) {
        super(SysConfig.class, forVariable(variable), "null", "sys_config");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.BIGINT));
        addMetadata(module, ColumnMetadata.named("module").withIndex(2).ofType(Types.VARCHAR));
        addMetadata(name, ColumnMetadata.named("name").withIndex(3).ofType(Types.VARCHAR));
        addMetadata(value, ColumnMetadata.named("value").withIndex(4).ofType(Types.VARCHAR));
        addMetadata(description, ColumnMetadata.named("description").withIndex(5).ofType(Types.VARCHAR));
        addMetadata(creatorUid, ColumnMetadata.named("creator_uid").withIndex(6).ofType(Types.BIGINT));
        addMetadata(opUid, ColumnMetadata.named("op_uid").withIndex(7).ofType(Types.BIGINT));
        addMetadata(createdAt, ColumnMetadata.named("created_at").withIndex(8).ofType(Types.TIMESTAMP));
        addMetadata(updatedAt, ColumnMetadata.named("updated_at").withIndex(9).ofType(Types.TIMESTAMP));
    }
}
