package io.nchain.token.xswap.domain.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;
import com.querydsl.core.types.dsl.*;
import javax.annotation.Generated;
import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;
import java.util.Date;

/**
 * Created By Sugar
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QSwapConfig extends com.querydsl.sql.RelationalPathBase<SwapConfig> {

    private static final long serialVersionUID = -5789666812551461279L;

    public static final QSwapConfig qSwapConfig = new QSwapConfig("swap_config");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Integer> swapinOn = createNumber("swapinOn", Integer.class);

    public final NumberPath<Integer> swapoutOn = createNumber("swapoutOn", Integer.class);

    public final NumberPath<Integer> displayOrder = createNumber("displayOrder", Integer.class);

    public final StringPath thirdChain = createString("thirdChain");

    public final StringPath thirdCoinSymbol = createString("thirdCoinSymbol");

    public final StringPath thirdCoinSymbolDisplay = createString("thirdCoinSymbolDisplay");

    public final StringPath thirdCoinLogo = createString("thirdCoinLogo");

    public final StringPath ownChain = createString("ownChain");

    public final StringPath ownCoinSymbol = createString("ownCoinSymbol");

    public final StringPath ownCoinSymbolDisplay = createString("ownCoinSymbolDisplay");

    public final StringPath ownCoinLogo = createString("ownCoinLogo");

    public final DateTimePath<Date> createdAt = createDateTime("createdAt", Date.class);

    public final DateTimePath<Date> updatedAt = createDateTime("updatedAt", Date.class);

    public final com.querydsl.sql.PrimaryKey<SwapConfig> primary = createPrimaryKey(id);

    public QSwapConfig(String variable) {
        super(SwapConfig.class, forVariable(variable), "null", "swap_config");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.BIGINT));
        addMetadata(swapinOn, ColumnMetadata.named("swapin_on").withIndex(2).ofType(Types.INTEGER));
        addMetadata(swapoutOn, ColumnMetadata.named("swapout_on").withIndex(3).ofType(Types.INTEGER));
        addMetadata(displayOrder, ColumnMetadata.named("display_order").withIndex(4).ofType(Types.INTEGER));
        addMetadata(thirdChain, ColumnMetadata.named("third_chain").withIndex(5).ofType(Types.VARCHAR));
        addMetadata(thirdCoinSymbol, ColumnMetadata.named("third_coin_symbol").withIndex(6).ofType(Types.VARCHAR));
        addMetadata(thirdCoinSymbolDisplay, ColumnMetadata.named("third_coin_symbol_display").withIndex(7).ofType(Types.VARCHAR));
        addMetadata(thirdCoinLogo, ColumnMetadata.named("third_coin_logo").withIndex(8).ofType(Types.VARCHAR));
        addMetadata(ownChain, ColumnMetadata.named("own_chain").withIndex(9).ofType(Types.VARCHAR));
        addMetadata(ownCoinSymbol, ColumnMetadata.named("own_coin_symbol").withIndex(10).ofType(Types.VARCHAR));
        addMetadata(ownCoinSymbolDisplay, ColumnMetadata.named("own_coin_symbol_display").withIndex(11).ofType(Types.VARCHAR));
        addMetadata(ownCoinLogo, ColumnMetadata.named("own_coin_logo").withIndex(12).ofType(Types.VARCHAR));
        addMetadata(createdAt, ColumnMetadata.named("created_at").withIndex(13).ofType(Types.TIMESTAMP));
        addMetadata(updatedAt, ColumnMetadata.named("updated_at").withIndex(14).ofType(Types.TIMESTAMP));
    }
}
