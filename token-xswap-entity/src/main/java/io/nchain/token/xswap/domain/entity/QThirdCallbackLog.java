package io.nchain.token.xswap.domain.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;
import com.querydsl.core.types.dsl.*;
import javax.annotation.Generated;
import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;
import java.util.Date;

/**
 * Created By Sugar
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QThirdCallbackLog extends com.querydsl.sql.RelationalPathBase<ThirdCallbackLog> {

    private static final long serialVersionUID = 9158578186927381354L;

    public static final QThirdCallbackLog qThirdCallbackLog = new QThirdCallbackLog("third_callback_log");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Integer> direction = createNumber("direction", Integer.class);

    public final NumberPath<Integer> depositStatus = createNumber("depositStatus", Integer.class);

    public final StringPath walletAddr = createString("walletAddr");

    public final NumberPath<Integer> depositOrderId = createNumber("depositOrderId", Integer.class);

    public final StringPath depositChain = createString("depositChain");

    public final StringPath depositCoinSymbol = createString("depositCoinSymbol");

    public final StringPath depositTxid = createString("depositTxid");

    public final NumberPath<java.math.BigDecimal> depositAmount = createNumber("depositAmount", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> depositBalance = createNumber("depositBalance", java.math.BigDecimal.class);

    public final DateTimePath<Date> depositConfirmedAt = createDateTime("depositConfirmedAt", Date.class);

    public final StringPath depositFromAddr = createString("depositFromAddr");

    public final StringPath depositThirdAddr = createString("depositThirdAddr");

    public final StringPath depositMemo = createString("depositMemo");

    public final NumberPath<Long> withdrawOrderId = createNumber("withdrawOrderId", Long.class);

    public final StringPath withdrawChain = createString("withdrawChain");

    public final StringPath withdrawCoinSymbol = createString("withdrawCoinSymbol");

    public final StringPath withdrawFromAddr = createString("withdrawFromAddr");

    public final StringPath withdrawToAddr = createString("withdrawToAddr");

    public final StringPath feeCoin = createString("feeCoin");

    public final StringPath thirdFeeAmount = createString("thirdFeeAmount");

    public final NumberPath<java.math.BigDecimal> exchangeRate = createNumber("exchangeRate", java.math.BigDecimal.class);

    public final StringPath ownFeeAmount = createString("ownFeeAmount");

    public final NumberPath<Integer> gas = createNumber("gas", Integer.class);

    public final NumberPath<java.math.BigDecimal> withdrawAmount = createNumber("withdrawAmount", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> withdrawAmountSent = createNumber("withdrawAmountSent", java.math.BigDecimal.class);

    public final StringPath withdrawTxid = createString("withdrawTxid");

    public final DateTimePath<Date> withdrawOrderCreatedAt = createDateTime("withdrawOrderCreatedAt", Date.class);

    public final NumberPath<Integer> withdrawStatus = createNumber("withdrawStatus", Integer.class);

    public final StringPath statusDesc = createString("statusDesc");

    public final StringPath withdrawMemo = createString("withdrawMemo");

    public final DateTimePath<Date> createdAt = createDateTime("createdAt", Date.class);

    public final DateTimePath<Date> updatedAt = createDateTime("updatedAt", Date.class);

    public final com.querydsl.sql.PrimaryKey<ThirdCallbackLog> primary = createPrimaryKey(id);

    public QThirdCallbackLog(String variable) {
        super(ThirdCallbackLog.class, forVariable(variable), "null", "third_callback_log");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.BIGINT));
        addMetadata(direction, ColumnMetadata.named("direction").withIndex(2).ofType(Types.INTEGER));
        addMetadata(depositStatus, ColumnMetadata.named("deposit_status").withIndex(3).ofType(Types.INTEGER));
        addMetadata(walletAddr, ColumnMetadata.named("wallet_addr").withIndex(4).ofType(Types.VARCHAR));
        addMetadata(depositOrderId, ColumnMetadata.named("deposit_order_id").withIndex(5).ofType(Types.INTEGER));
        addMetadata(depositChain, ColumnMetadata.named("deposit_chain").withIndex(6).ofType(Types.VARCHAR));
        addMetadata(depositCoinSymbol, ColumnMetadata.named("deposit_coin_symbol").withIndex(7).ofType(Types.VARCHAR));
        addMetadata(depositTxid, ColumnMetadata.named("deposit_txid").withIndex(8).ofType(Types.VARCHAR));
        addMetadata(depositAmount, ColumnMetadata.named("deposit_amount").withIndex(9).ofType(Types.DECIMAL));
        addMetadata(depositBalance, ColumnMetadata.named("deposit_balance").withIndex(10).ofType(Types.DECIMAL));
        addMetadata(depositConfirmedAt, ColumnMetadata.named("deposit_confirmed_at").withIndex(11).ofType(Types.TIMESTAMP));
        addMetadata(depositFromAddr, ColumnMetadata.named("deposit_from_addr").withIndex(12).ofType(Types.VARCHAR));
        addMetadata(depositThirdAddr, ColumnMetadata.named("deposit_third_addr").withIndex(13).ofType(Types.VARCHAR));
        addMetadata(depositMemo, ColumnMetadata.named("deposit_memo").withIndex(14).ofType(Types.VARCHAR));
        addMetadata(withdrawOrderId, ColumnMetadata.named("withdraw_order_id").withIndex(15).ofType(Types.BIGINT));
        addMetadata(withdrawChain, ColumnMetadata.named("withdraw_chain").withIndex(16).ofType(Types.VARCHAR));
        addMetadata(withdrawCoinSymbol, ColumnMetadata.named("withdraw_coin_symbol").withIndex(17).ofType(Types.VARCHAR));
        addMetadata(withdrawFromAddr, ColumnMetadata.named("withdraw_from_addr").withIndex(18).ofType(Types.VARCHAR));
        addMetadata(withdrawToAddr, ColumnMetadata.named("withdraw_to_addr").withIndex(19).ofType(Types.VARCHAR));
        addMetadata(feeCoin, ColumnMetadata.named("fee_coin").withIndex(20).ofType(Types.VARCHAR));
        addMetadata(thirdFeeAmount, ColumnMetadata.named("third_fee_amount").withIndex(21).ofType(Types.VARCHAR));
        addMetadata(exchangeRate, ColumnMetadata.named("exchange_rate").withIndex(22).ofType(Types.DECIMAL));
        addMetadata(ownFeeAmount, ColumnMetadata.named("own_fee_amount").withIndex(23).ofType(Types.VARCHAR));
        addMetadata(gas, ColumnMetadata.named("gas").withIndex(24).ofType(Types.INTEGER));
        addMetadata(withdrawAmount, ColumnMetadata.named("withdraw_amount").withIndex(25).ofType(Types.DECIMAL));
        addMetadata(withdrawAmountSent, ColumnMetadata.named("withdraw_amount_sent").withIndex(26).ofType(Types.DECIMAL));
        addMetadata(withdrawTxid, ColumnMetadata.named("withdraw_txid").withIndex(27).ofType(Types.VARCHAR));
        addMetadata(withdrawOrderCreatedAt, ColumnMetadata.named("withdraw_order_created_at").withIndex(28).ofType(Types.TIMESTAMP));
        addMetadata(withdrawStatus, ColumnMetadata.named("withdraw_status").withIndex(29).ofType(Types.INTEGER));
        addMetadata(statusDesc, ColumnMetadata.named("status_desc").withIndex(30).ofType(Types.VARCHAR));
        addMetadata(withdrawMemo, ColumnMetadata.named("withdraw_memo").withIndex(31).ofType(Types.VARCHAR));
        addMetadata(createdAt, ColumnMetadata.named("created_at").withIndex(32).ofType(Types.TIMESTAMP));
        addMetadata(updatedAt, ColumnMetadata.named("updated_at").withIndex(33).ofType(Types.TIMESTAMP));
    }
}
