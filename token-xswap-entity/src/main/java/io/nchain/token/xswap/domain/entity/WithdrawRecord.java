package io.nchain.token.xswap.domain.entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@SuppressWarnings("ALL")
@Entity
@DynamicInsert
@DynamicUpdate
public class WithdrawRecord {

    @Id
    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    @Column
    private long id;

    @Column
    private int withdrawOrderId;

    @Column
    private String walletAddr;

    @Column
    private String ownerChain;

    @Column
    private String coinSymbol;

    @Column
    private java.math.BigDecimal rate;

    @Column
    private java.math.BigDecimal amount;

    @Column
    private java.math.BigDecimal amountSent;

    @Column
    private String txid;

    @Column
    private String feeCoin;

    @Column
    private java.math.BigDecimal feeAmount;

    @Column
    private Date orderCreatedTime;

    @Column
    private int status;

    @Column
    private String statusDesc;

    @Column
    private String memo;

    @Column
    private Date createdAt;

    @Column
    private Date updated;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getWithdrawOrderId() {
        return withdrawOrderId;
    }

    public void setWithdrawOrderId(int withdrawOrderId) {
        this.withdrawOrderId = withdrawOrderId;
    }

    public String getWalletAddr() {
        return walletAddr;
    }

    public void setWalletAddr(String walletAddr) {
        this.walletAddr = walletAddr;
    }

    public String getOwnerChain() {
        return ownerChain;
    }

    public void setOwnerChain(String ownerChain) {
        this.ownerChain = ownerChain;
    }

    public String getCoinSymbol() {
        return coinSymbol;
    }

    public void setCoinSymbol(String coinSymbol) {
        this.coinSymbol = coinSymbol;
    }

    public java.math.BigDecimal getRate() {
        return rate;
    }

    public void setRate(java.math.BigDecimal rate) {
        this.rate = rate;
    }

    public java.math.BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(java.math.BigDecimal amount) {
        this.amount = amount;
    }

    public java.math.BigDecimal getAmountSent() {
        return amountSent;
    }

    public void setAmountSent(java.math.BigDecimal amountSent) {
        this.amountSent = amountSent;
    }

    public String getTxid() {
        return txid;
    }

    public void setTxid(String txid) {
        this.txid = txid;
    }

    public String getFeeCoin() {
        return feeCoin;
    }

    public void setFeeCoin(String feeCoin) {
        this.feeCoin = feeCoin;
    }

    public java.math.BigDecimal getFeeAmount() {
        return feeAmount;
    }

    public void setFeeAmount(java.math.BigDecimal feeAmount) {
        this.feeAmount = feeAmount;
    }

    public Date getOrderCreatedTime() {
        return orderCreatedTime;
    }

    public void setOrderCreatedTime(Date orderCreatedTime) {
        this.orderCreatedTime = orderCreatedTime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

}
