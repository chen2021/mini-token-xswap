package io.nchain.token.xswap.domain.repository;

import io.nchain.token.xswap.domain.entity.SysConfig;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import io.nchain.token.xswap.domain.entity.ExchangeRate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExchangeRateRepository extends JpaRepository<ExchangeRate, Long>,
        QuerydslPredicateExecutor<ExchangeRate> {
    @Query(value = "select * from exchange_rate where exchange_pair=?",nativeQuery = true)
    ExchangeRate findByName(@NotNull String exchangePair);
}
