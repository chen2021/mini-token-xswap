package io.nchain.token.xswap.domain.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;
import com.querydsl.core.types.dsl.*;
import javax.annotation.Generated;
import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;
import java.util.Date;

/**
 * Created By Sugar
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QChainConfig extends com.querydsl.sql.RelationalPathBase<ChainConfig> {

    private static final long serialVersionUID = -8143768956950668879L;

    public static final QChainConfig qChainConfig = new QChainConfig("chain_config");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath chain = createString("chain");

    public final NumberPath<Integer> gas = createNumber("gas", Integer.class);

    public final com.querydsl.sql.PrimaryKey<ChainConfig> primary = createPrimaryKey(id);

    public QChainConfig(String variable) {
        super(ChainConfig.class, forVariable(variable), "null", "chain_config");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.BIGINT));
        addMetadata(chain, ColumnMetadata.named("chain").withIndex(2).ofType(Types.VARCHAR));
        addMetadata(gas, ColumnMetadata.named("gas").withIndex(3).ofType(Types.INTEGER));
    }
}
