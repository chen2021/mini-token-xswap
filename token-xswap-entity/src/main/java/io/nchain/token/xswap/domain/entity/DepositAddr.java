package io.nchain.token.xswap.domain.entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@SuppressWarnings("ALL")
@Entity
@DynamicInsert
@DynamicUpdate
public class DepositAddr {

    @Id
    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    @Column
    private long id;

    @Column
    private String walletAddr;

    @Column
    private int direction;

    @Column
    private String ownChain;

    @Column
    private String ownCoinSymbol;

    @Column
    private String thirdAddr;

    @Column
    private String thirdChain;

    @Column
    private String thirdCoinSymbol;

    @Column
    private String depositMemo;

    @Column
    private String ip;

    @Column
    private String ipInfo;

    @Column
    private Date createdAt;

    @Column
    private Date updatedAt;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getWalletAddr() {
        return walletAddr;
    }

    public void setWalletAddr(String walletAddr) {
        this.walletAddr = walletAddr;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public String getOwnChain() {
        return ownChain;
    }

    public void setOwnChain(String ownChain) {
        this.ownChain = ownChain;
    }

    public String getOwnCoinSymbol() {
        return ownCoinSymbol;
    }

    public void setOwnCoinSymbol(String ownCoinSymbol) {
        this.ownCoinSymbol = ownCoinSymbol;
    }

    public String getThirdAddr() {
        return thirdAddr;
    }

    public void setThirdAddr(String thirdAddr) {
        this.thirdAddr = thirdAddr;
    }

    public String getThirdChain() {
        return thirdChain;
    }

    public void setThirdChain(String thirdChain) {
        this.thirdChain = thirdChain;
    }

    public String getThirdCoinSymbol() {
        return thirdCoinSymbol;
    }

    public void setThirdCoinSymbol(String thirdCoinSymbol) {
        this.thirdCoinSymbol = thirdCoinSymbol;
    }

    public String getDepositMemo() {
        return depositMemo;
    }

    public void setDepositMemo(String depositMemo) {
        this.depositMemo = depositMemo;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getIpInfo() {
        return ipInfo;
    }

    public void setIpInfo(String ipInfo) {
        this.ipInfo = ipInfo;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

}
