package io.nchain.token.xswap.domain.repository;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import io.nchain.token.xswap.domain.entity.WithdrawRecord;
import org.springframework.data.jpa.repository.JpaRepository;

@SuppressWarnings("ALL")
public interface WithdrawRecordRepository extends JpaRepository<WithdrawRecord, Long>,
        QuerydslPredicateExecutor<WithdrawRecord> {
}
