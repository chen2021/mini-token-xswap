package io.nchain.token.xswap.domain.entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Entity
@DynamicInsert
@DynamicUpdate
public class SwapConfig {

    @Id
    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    @Column
    private long id;

    @Column
    private int swapinOn;

    @Column
    private int swapoutOn;

    @Column
    private int displayOrder;

    @Column
    private String thirdChain;

    @Column
    private String thirdCoinSymbol;

    @Column
    private String thirdCoinSymbolDisplay;

    @Column
    private String thirdCoinLogo;

    @Column
    private String ownChain;

    @Column
    private String ownCoinSymbol;

    @Column
    private String ownCoinSymbolDisplay;

    @Column
    private String ownCoinLogo;

    @Column
    private Date createdAt;

    @Column
    private Date updatedAt;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getSwapinOn() {
        return swapinOn;
    }

    public void setSwapinOn(int swapinOn) {
        this.swapinOn = swapinOn;
    }

    public int getSwapoutOn() {
        return swapoutOn;
    }

    public void setSwapoutOn(int swapoutOn) {
        this.swapoutOn = swapoutOn;
    }

    public int getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }

    public String getThirdChain() {
        return thirdChain;
    }

    public void setThirdChain(String thirdChain) {
        this.thirdChain = thirdChain;
    }

    public String getThirdCoinSymbol() {
        return thirdCoinSymbol;
    }

    public void setThirdCoinSymbol(String thirdCoinSymbol) {
        this.thirdCoinSymbol = thirdCoinSymbol;
    }

    public String getThirdCoinSymbolDisplay() {
        return thirdCoinSymbolDisplay;
    }

    public void setThirdCoinSymbolDisplay(String thirdCoinSymbolDisplay) {
        this.thirdCoinSymbolDisplay = thirdCoinSymbolDisplay;
    }

    public String getThirdCoinLogo() {
        return thirdCoinLogo;
    }

    public void setThirdCoinLogo(String thirdCoinLogo) {
        this.thirdCoinLogo = thirdCoinLogo;
    }

    public String getOwnChain() {
        return ownChain;
    }

    public void setOwnChain(String ownChain) {
        this.ownChain = ownChain;
    }

    public String getOwnCoinSymbol() {
        return ownCoinSymbol;
    }

    public void setOwnCoinSymbol(String ownCoinSymbol) {
        this.ownCoinSymbol = ownCoinSymbol;
    }

    public String getOwnCoinSymbolDisplay() {
        return ownCoinSymbolDisplay;
    }

    public void setOwnCoinSymbolDisplay(String ownCoinSymbolDisplay) {
        this.ownCoinSymbolDisplay = ownCoinSymbolDisplay;
    }

    public String getOwnCoinLogo() {
        return ownCoinLogo;
    }

    public void setOwnCoinLogo(String ownCoinLogo) {
        this.ownCoinLogo = ownCoinLogo;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

}
