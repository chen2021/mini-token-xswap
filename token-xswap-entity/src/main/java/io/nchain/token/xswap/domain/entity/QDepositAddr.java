package io.nchain.token.xswap.domain.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;
import com.querydsl.core.types.dsl.*;
import javax.annotation.Generated;
import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;
import java.util.Date;

/**
 * Created By Sugar
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QDepositAddr extends com.querydsl.sql.RelationalPathBase<DepositAddr> {

    private static final long serialVersionUID = -271574970176457321L;

    public static final QDepositAddr qDepositAddr = new QDepositAddr("deposit_addr");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath walletAddr = createString("walletAddr");

    public final NumberPath<Integer> direction = createNumber("direction", Integer.class);

    public final StringPath ownChain = createString("ownChain");

    public final StringPath ownCoinSymbol = createString("ownCoinSymbol");

    public final StringPath thirdAddr = createString("thirdAddr");

    public final StringPath thirdChain = createString("thirdChain");

    public final StringPath thirdCoinSymbol = createString("thirdCoinSymbol");

    public final StringPath depositMemo = createString("depositMemo");

    public final StringPath ip = createString("ip");

    public final StringPath ipInfo = createString("ipInfo");

    public final DateTimePath<Date> createdAt = createDateTime("createdAt", Date.class);

    public final DateTimePath<Date> updatedAt = createDateTime("updatedAt", Date.class);

    public final com.querydsl.sql.PrimaryKey<DepositAddr> primary = createPrimaryKey(id);

    public QDepositAddr(String variable) {
        super(DepositAddr.class, forVariable(variable), "null", "deposit_addr");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.BIGINT));
        addMetadata(walletAddr, ColumnMetadata.named("wallet_addr").withIndex(2).ofType(Types.VARCHAR));
        addMetadata(direction, ColumnMetadata.named("direction").withIndex(3).ofType(Types.INTEGER));
        addMetadata(ownChain, ColumnMetadata.named("own_chain").withIndex(4).ofType(Types.VARCHAR));
        addMetadata(ownCoinSymbol, ColumnMetadata.named("own_coin_symbol").withIndex(5).ofType(Types.VARCHAR));
        addMetadata(thirdAddr, ColumnMetadata.named("third_addr").withIndex(6).ofType(Types.VARCHAR));
        addMetadata(thirdChain, ColumnMetadata.named("third_chain").withIndex(7).ofType(Types.VARCHAR));
        addMetadata(thirdCoinSymbol, ColumnMetadata.named("third_coin_symbol").withIndex(8).ofType(Types.VARCHAR));
        addMetadata(depositMemo, ColumnMetadata.named("deposit_memo").withIndex(9).ofType(Types.VARCHAR));
        addMetadata(ip, ColumnMetadata.named("ip").withIndex(10).ofType(Types.VARCHAR));
        addMetadata(ipInfo, ColumnMetadata.named("ip_info").withIndex(11).ofType(Types.VARCHAR));
        addMetadata(createdAt, ColumnMetadata.named("created_at").withIndex(12).ofType(Types.TIMESTAMP));
        addMetadata(updatedAt, ColumnMetadata.named("updated_at").withIndex(13).ofType(Types.TIMESTAMP));
    }
}
