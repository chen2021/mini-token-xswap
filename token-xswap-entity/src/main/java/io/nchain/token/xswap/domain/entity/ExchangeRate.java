package io.nchain.token.xswap.domain.entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Entity
@DynamicInsert
@DynamicUpdate
public class ExchangeRate {

    @Id
    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    @Column
    private int id;

    @Column
    private String exchangePair;

    @Column
    private java.math.BigDecimal exchageRate;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getExchangePair() {
        return exchangePair;
    }

    public void setExchangePair(String exchangePair) {
        this.exchangePair = exchangePair;
    }

    public java.math.BigDecimal getExchageRate() {
        return exchageRate;
    }

    public void setExchageRate(java.math.BigDecimal exchageRate) {
        this.exchageRate = exchageRate;
    }

}
