package io.nchain.token.xswap.domain.entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@SuppressWarnings("ALL")
@Entity
@DynamicInsert
@DynamicUpdate
public class SysConfig {

    @Id
    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    @Column
    private long id;

    @Column
    private String module;

    @Column
    private String name;

    @Column
    private String value;

    @Column
    private String description;

    @Column
    private long creatorUid;

    @Column
    private long opUid;

    @Column
    private Date createdAt;

    @Column
    private Date updatedAt;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getCreatorUid() {
        return creatorUid;
    }

    public void setCreatorUid(long creatorUid) {
        this.creatorUid = creatorUid;
    }

    public long getOpUid() {
        return opUid;
    }

    public void setOpUid(long opUid) {
        this.opUid = opUid;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

}
