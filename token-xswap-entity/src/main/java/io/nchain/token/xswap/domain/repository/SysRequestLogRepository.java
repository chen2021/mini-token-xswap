package io.nchain.token.xswap.domain.repository;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import io.nchain.token.xswap.domain.entity.SysRequestLog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SysRequestLogRepository extends JpaRepository<SysRequestLog, Long>,
        QuerydslPredicateExecutor<SysRequestLog> {
}
