package io.nchain.token.xswap.domain.repository;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import io.nchain.token.xswap.domain.entity.SwapConfig;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SwapConfigRepository extends JpaRepository<SwapConfig, Long>,
        QuerydslPredicateExecutor<SwapConfig> {
}
