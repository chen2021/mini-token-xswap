package io.nchain.token.xswap.domain.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;
import com.querydsl.core.types.dsl.*;
import javax.annotation.Generated;
import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;
import java.util.Date;

/**
 * Created By Sugar
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QWithdrawRecord extends com.querydsl.sql.RelationalPathBase<WithdrawRecord> {

    private static final long serialVersionUID = -4318287356299655770L;

    public static final QWithdrawRecord qWithdrawRecord = new QWithdrawRecord("withdraw_record");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Integer> withdrawOrderId = createNumber("withdrawOrderId", Integer.class);

    public final StringPath walletAddr = createString("walletAddr");

    public final StringPath ownerChain = createString("ownerChain");

    public final StringPath coinSymbol = createString("coinSymbol");

    public final NumberPath<java.math.BigDecimal> rate = createNumber("rate", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> amount = createNumber("amount", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> amountSent = createNumber("amountSent", java.math.BigDecimal.class);

    public final StringPath txid = createString("txid");

    public final StringPath feeCoin = createString("feeCoin");

    public final NumberPath<java.math.BigDecimal> feeAmount = createNumber("feeAmount", java.math.BigDecimal.class);

    public final DateTimePath<Date> orderCreatedTime = createDateTime("orderCreatedTime", Date.class);

    public final NumberPath<Integer> status = createNumber("status", Integer.class);

    public final StringPath statusDesc = createString("statusDesc");

    public final StringPath memo = createString("memo");

    public final DateTimePath<Date> createdAt = createDateTime("createdAt", Date.class);

    public final DateTimePath<Date> updated = createDateTime("updated", Date.class);

    public final com.querydsl.sql.PrimaryKey<WithdrawRecord> primary = createPrimaryKey(id);

    public QWithdrawRecord(String variable) {
        super(WithdrawRecord.class, forVariable(variable), "null", "withdraw_record");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.BIGINT));
        addMetadata(withdrawOrderId, ColumnMetadata.named("withdraw_order_id").withIndex(2).ofType(Types.INTEGER));
        addMetadata(walletAddr, ColumnMetadata.named("wallet_addr").withIndex(3).ofType(Types.VARCHAR));
        addMetadata(ownerChain, ColumnMetadata.named("owner_chain").withIndex(4).ofType(Types.VARCHAR));
        addMetadata(coinSymbol, ColumnMetadata.named("coin_symbol").withIndex(5).ofType(Types.VARCHAR));
        addMetadata(rate, ColumnMetadata.named("rate").withIndex(6).ofType(Types.DECIMAL));
        addMetadata(amount, ColumnMetadata.named("amount").withIndex(7).ofType(Types.DECIMAL));
        addMetadata(amountSent, ColumnMetadata.named("amount_sent").withIndex(8).ofType(Types.DECIMAL));
        addMetadata(txid, ColumnMetadata.named("txid").withIndex(9).ofType(Types.VARCHAR));
        addMetadata(feeCoin, ColumnMetadata.named("fee_coin").withIndex(10).ofType(Types.VARCHAR));
        addMetadata(feeAmount, ColumnMetadata.named("fee_amount").withIndex(11).ofType(Types.DECIMAL));
        addMetadata(orderCreatedTime, ColumnMetadata.named("order_created_time").withIndex(12).ofType(Types.TIMESTAMP));
        addMetadata(status, ColumnMetadata.named("status").withIndex(13).ofType(Types.INTEGER));
        addMetadata(statusDesc, ColumnMetadata.named("status_desc").withIndex(14).ofType(Types.VARCHAR));
        addMetadata(memo, ColumnMetadata.named("memo").withIndex(15).ofType(Types.VARCHAR));
        addMetadata(createdAt, ColumnMetadata.named("created_at").withIndex(16).ofType(Types.TIMESTAMP));
        addMetadata(updated, ColumnMetadata.named("updated").withIndex(17).ofType(Types.TIMESTAMP));
    }
}
