package io.nchain.token.xswap.domain.repository;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import io.nchain.token.xswap.domain.entity.ChainConfig;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ChainConfigRepository extends JpaRepository<ChainConfig, Long>,
        QuerydslPredicateExecutor<ChainConfig> {
}
