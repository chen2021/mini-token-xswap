package io.nchain.token.xswap.domain.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import io.nchain.token.xswap.domain.entity.SysConfig;
import org.springframework.data.jpa.repository.JpaRepository;

@SuppressWarnings("ALL")
public interface SysConfigRepository extends JpaRepository<SysConfig, Long>,
        QuerydslPredicateExecutor<SysConfig> {
    @Query(value = "select * from sys_config where name=?",nativeQuery = true)
    SysConfig findByName(@NotNull String cfgName);
}
