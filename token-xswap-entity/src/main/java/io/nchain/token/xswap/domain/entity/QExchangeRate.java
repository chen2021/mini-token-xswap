package io.nchain.token.xswap.domain.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;
import com.querydsl.core.types.dsl.*;
import javax.annotation.Generated;
import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;
import java.util.Date;

/**
 * Created By Sugar
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QExchangeRate extends com.querydsl.sql.RelationalPathBase<ExchangeRate> {

    private static final long serialVersionUID = 2103139134576285307L;

    public static final QExchangeRate qExchangeRate = new QExchangeRate("exchange_rate");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath exchangePair = createString("exchangePair");

    public final NumberPath<java.math.BigDecimal> exchageRate = createNumber("exchageRate", java.math.BigDecimal.class);

    public final com.querydsl.sql.PrimaryKey<ExchangeRate> primary = createPrimaryKey(id);

    public QExchangeRate(String variable) {
        super(ExchangeRate.class, forVariable(variable), "null", "exchange_rate");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.INTEGER));
        addMetadata(exchangePair, ColumnMetadata.named("exchange_pair").withIndex(2).ofType(Types.VARCHAR));
        addMetadata(exchageRate, ColumnMetadata.named("exchage_rate").withIndex(3).ofType(Types.DECIMAL));
    }
}
