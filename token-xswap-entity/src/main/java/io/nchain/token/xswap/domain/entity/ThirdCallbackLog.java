package io.nchain.token.xswap.domain.entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Entity
@DynamicInsert
@DynamicUpdate
public class ThirdCallbackLog {

    @Id
    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    @Column
    private long id;

    @Column
    private int direction;

    @Column
    private int depositStatus;

    @Column
    private String walletAddr;

    @Column
    private int depositOrderId;

    @Column
    private String depositChain;

    @Column
    private String depositCoinSymbol;

    @Column
    private String depositTxid;

    @Column
    private java.math.BigDecimal depositAmount;

    @Column
    private java.math.BigDecimal depositBalance;

    @Column
    private Date depositConfirmedAt;

    @Column
    private String depositFromAddr;

    @Column
    private String depositThirdAddr;

    @Column
    private String depositMemo;

    @Column
    private long withdrawOrderId;

    @Column
    private String withdrawChain;

    @Column
    private String withdrawCoinSymbol;

    @Column
    private String withdrawFromAddr;

    @Column
    private String withdrawToAddr;

    @Column
    private String feeCoin;

    @Column
    private String thirdFeeAmount;

    @Column
    private java.math.BigDecimal exchangeRate;

    @Column
    private String ownFeeAmount;

    @Column
    private int gas;

    @Column
    private java.math.BigDecimal withdrawAmount;

    @Column
    private java.math.BigDecimal withdrawAmountSent;

    @Column
    private String withdrawTxid;

    @Column
    private Date withdrawOrderCreatedAt;

    @Column
    private int withdrawStatus;

    @Column
    private String statusDesc;

    @Column
    private String withdrawMemo;

    @Column
    private Date createdAt;

    @Column
    private Date updatedAt;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public int getDepositStatus() {
        return depositStatus;
    }

    public void setDepositStatus(int depositStatus) {
        this.depositStatus = depositStatus;
    }

    public String getWalletAddr() {
        return walletAddr;
    }

    public void setWalletAddr(String walletAddr) {
        this.walletAddr = walletAddr;
    }

    public int getDepositOrderId() {
        return depositOrderId;
    }

    public void setDepositOrderId(int depositOrderId) {
        this.depositOrderId = depositOrderId;
    }

    public String getDepositChain() {
        return depositChain;
    }

    public void setDepositChain(String depositChain) {
        this.depositChain = depositChain;
    }

    public String getDepositCoinSymbol() {
        return depositCoinSymbol;
    }

    public void setDepositCoinSymbol(String depositCoinSymbol) {
        this.depositCoinSymbol = depositCoinSymbol;
    }

    public String getDepositTxid() {
        return depositTxid;
    }

    public void setDepositTxid(String depositTxid) {
        this.depositTxid = depositTxid;
    }

    public java.math.BigDecimal getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(java.math.BigDecimal depositAmount) {
        this.depositAmount = depositAmount;
    }

    public java.math.BigDecimal getDepositBalance() {
        return depositBalance;
    }

    public void setDepositBalance(java.math.BigDecimal depositBalance) {
        this.depositBalance = depositBalance;
    }

    public Date getDepositConfirmedAt() {
        return depositConfirmedAt;
    }

    public void setDepositConfirmedAt(Date depositConfirmedAt) {
        this.depositConfirmedAt = depositConfirmedAt;
    }

    public String getDepositFromAddr() {
        return depositFromAddr;
    }

    public void setDepositFromAddr(String depositFromAddr) {
        this.depositFromAddr = depositFromAddr;
    }

    public String getDepositThirdAddr() {
        return depositThirdAddr;
    }

    public void setDepositThirdAddr(String depositThirdAddr) {
        this.depositThirdAddr = depositThirdAddr;
    }

    public String getDepositMemo() {
        return depositMemo;
    }

    public void setDepositMemo(String depositMemo) {
        this.depositMemo = depositMemo;
    }

    public long getWithdrawOrderId() {
        return withdrawOrderId;
    }

    public void setWithdrawOrderId(long withdrawOrderId) {
        this.withdrawOrderId = withdrawOrderId;
    }

    public String getWithdrawChain() {
        return withdrawChain;
    }

    public void setWithdrawChain(String withdrawChain) {
        this.withdrawChain = withdrawChain;
    }

    public String getWithdrawCoinSymbol() {
        return withdrawCoinSymbol;
    }

    public void setWithdrawCoinSymbol(String withdrawCoinSymbol) {
        this.withdrawCoinSymbol = withdrawCoinSymbol;
    }

    public String getWithdrawFromAddr() {
        return withdrawFromAddr;
    }

    public void setWithdrawFromAddr(String withdrawFromAddr) {
        this.withdrawFromAddr = withdrawFromAddr;
    }

    public String getWithdrawToAddr() {
        return withdrawToAddr;
    }

    public void setWithdrawToAddr(String withdrawToAddr) {
        this.withdrawToAddr = withdrawToAddr;
    }

    public String getFeeCoin() {
        return feeCoin;
    }

    public void setFeeCoin(String feeCoin) {
        this.feeCoin = feeCoin;
    }

    public String getThirdFeeAmount() {
        return thirdFeeAmount;
    }

    public void setThirdFeeAmount(String thirdFeeAmount) {
        this.thirdFeeAmount = thirdFeeAmount;
    }

    public java.math.BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(java.math.BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public String getOwnFeeAmount() {
        return ownFeeAmount;
    }

    public void setOwnFeeAmount(String ownFeeAmount) {
        this.ownFeeAmount = ownFeeAmount;
    }

    public int getGas() {
        return gas;
    }

    public void setGas(int gas) {
        this.gas = gas;
    }

    public java.math.BigDecimal getWithdrawAmount() {
        return withdrawAmount;
    }

    public void setWithdrawAmount(java.math.BigDecimal withdrawAmount) {
        this.withdrawAmount = withdrawAmount;
    }

    public java.math.BigDecimal getWithdrawAmountSent() {
        return withdrawAmountSent;
    }

    public void setWithdrawAmountSent(java.math.BigDecimal withdrawAmountSent) {
        this.withdrawAmountSent = withdrawAmountSent;
    }

    public String getWithdrawTxid() {
        return withdrawTxid;
    }

    public void setWithdrawTxid(String withdrawTxid) {
        this.withdrawTxid = withdrawTxid;
    }

    public Date getWithdrawOrderCreatedAt() {
        return withdrawOrderCreatedAt;
    }

    public void setWithdrawOrderCreatedAt(Date withdrawOrderCreatedAt) {
        this.withdrawOrderCreatedAt = withdrawOrderCreatedAt;
    }

    public int getWithdrawStatus() {
        return withdrawStatus;
    }

    public void setWithdrawStatus(int withdrawStatus) {
        this.withdrawStatus = withdrawStatus;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public String getWithdrawMemo() {
        return withdrawMemo;
    }

    public void setWithdrawMemo(String withdrawMemo) {
        this.withdrawMemo = withdrawMemo;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

}
