package io.nchain.token.xswap.domain.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import io.nchain.token.xswap.domain.entity.ThirdCallbackLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@SuppressWarnings("ALL")
public interface ThirdCallbackLogRepository extends JpaRepository<ThirdCallbackLog, Long>,
        QuerydslPredicateExecutor<ThirdCallbackLog> {
    @Modifying
    @Transactional
    @Query(value = "update third_callback_log set withdraw_amount=?1,withdraw_amount_sent=?2,withdraw_status=?3,status_desc=?4,withdraw_txid=?5,withdraw_from_addr=?6 where withdraw_order_id=?7 ", nativeQuery = true)
    void update(@Nullable BigDecimal withdrawAmount, @Nullable BigDecimal withdrawAmountSent, int withdrawStatus, @Nullable String statusDesc, @Nullable String withdrawTxid, String withdrawFromAddr, long withdrawOrderId);

    @Query(value = "select * from third_callback_log where id >=?1 limit ?2", nativeQuery = true)
    List<ThirdCallbackLog> select(@NotNull long index, @NotNull int size);
}
