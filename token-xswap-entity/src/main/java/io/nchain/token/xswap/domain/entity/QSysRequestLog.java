package io.nchain.token.xswap.domain.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;
import com.querydsl.core.types.dsl.*;
import javax.annotation.Generated;
import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;
import java.util.Date;

/**
 * Created By Sugar
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QSysRequestLog extends com.querydsl.sql.RelationalPathBase<SysRequestLog> {

    private static final long serialVersionUID = 4524835353241516130L;

    public static final QSysRequestLog qSysRequestLog = new QSysRequestLog("sys_request_log");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath ip = createString("ip");

    public final StringPath method = createString("method");

    public final StringPath uri = createString("uri");

    public final StringPath params = createString("params");

    public final StringPath requestBody = createString("requestBody");

    public final StringPath responseBody = createString("responseBody");

    public final DateTimePath<Date> createdAt = createDateTime("createdAt", Date.class);

    public final DateTimePath<Date> updatedAt = createDateTime("updatedAt", Date.class);

    public final com.querydsl.sql.PrimaryKey<SysRequestLog> primary = createPrimaryKey(id);

    public QSysRequestLog(String variable) {
        super(SysRequestLog.class, forVariable(variable), "null", "sys_request_log");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.BIGINT));
        addMetadata(ip, ColumnMetadata.named("ip").withIndex(2).ofType(Types.VARCHAR));
        addMetadata(method, ColumnMetadata.named("method").withIndex(3).ofType(Types.VARCHAR));
        addMetadata(uri, ColumnMetadata.named("uri").withIndex(4).ofType(Types.VARCHAR));
        addMetadata(params, ColumnMetadata.named("params").withIndex(5).ofType(Types.VARCHAR));
        addMetadata(requestBody, ColumnMetadata.named("request_body").withIndex(6).ofType(Types.VARCHAR));
        addMetadata(responseBody, ColumnMetadata.named("response_body").withIndex(7).ofType(Types.VARCHAR));
        addMetadata(createdAt, ColumnMetadata.named("created_at").withIndex(8).ofType(Types.TIMESTAMP));
        addMetadata(updatedAt, ColumnMetadata.named("updated_at").withIndex(9).ofType(Types.TIMESTAMP));
    }
}
