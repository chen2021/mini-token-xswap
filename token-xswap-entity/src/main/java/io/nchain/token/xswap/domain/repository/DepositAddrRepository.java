package io.nchain.token.xswap.domain.repository;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import io.nchain.token.xswap.domain.entity.DepositAddr;
import org.springframework.data.jpa.repository.JpaRepository;

@SuppressWarnings("ALL")
public interface DepositAddrRepository extends JpaRepository<DepositAddr, Long>,
        QuerydslPredicateExecutor<DepositAddr> {
}
