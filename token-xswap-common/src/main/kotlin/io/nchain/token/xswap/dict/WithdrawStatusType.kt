package io.nchain.token.xswap.dict

/**
 *
 * @author     : CHEN
 * @Date       : Created in 2021/4/7 7:33 下午
 * @description：
 * @modified By：
 * @Version    : 1.0
 **/
enum class WithdrawStatusType(val code: Int, val msg: String) {
    NO_VALID_TYPE(0, "无效状态"),
    READY_SEND(1, "准备发送"),
    SENDING(2, "发送中"),
    SEND_SUCCESSED(3, "发送成功"),
    SEND_FAILED(4, "发送失败"),
    WAIT_CONFIRMED(5, "待确认")
}