package io.nchain.token.xswap.domain.bean

/**
 * @author     ：CHEN
 * @date       ：Created in 2020/11/19 13:43
 * @description：
 * @modified By：
 * @version    ：1.0
 */
class InfoResult {
    var cryptype: Int = 0
    var data = InfoResultData()

    class InfoResultData {
        var eno: Int? = null
        var emsg: String? = null
        var data = ArrayList<SecData>()

        class SecData {
            var chain: String? = null
            var coin: String? = null
            var coin_precision: Int? = null
            var min_deposit_amount: String? = null
            var min_withdraw_amount: String? = null
            var deposit_enabled: Int? = null
            var withdraw_enabled: Int? = null
            var deposit_confirm_count: Int? = null
            var fee_coin: String? = null
            var fee_type: Int? = null  //0=固定数量,1=费率,2=混合(withdraw_amt*rate+amount)
            var fee_amount: String? = null
            var fee_rate: String? = null
            var need_memo: Int? = 0  //1=充值需要备注,0=充值不需要备注
        }
    }
}