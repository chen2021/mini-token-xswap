package io.nchain.token.xswap.dict

import com.waykichain.securitiesaccessories.commons.BaseError

/**
 * @author     ：CHEN
 * @date       ：Created in 2021/2/1 16:55
 * @description：
 * @modified By：
 * @version    ：1.0
 */
enum class ErrorCode(private val code: Int, private val msg: String) : BaseError {

    SYSYTEM_ERROR                           (1, "system error"),
    NO_ADMIN_PK                             (1000, "管理员私钥不存在"),
    WRONG_DIRECITION                        (1001,"刚兑方向错误"),
    WRONG_PWD                               (2000,"询问开发人员获取密码"),
    THIRTY_INTERFACE                        (3000, "第三方接口错误:【code:%d】【msg:%s】"),
    QUICK_SWAP                              (3001, "该订单已提交"),
    TOKEN_VERIFIY_FAILED                    (10001,"token verifiy failed");

    override fun getCode(): Int {
        return this.code
    }

    override fun getMsg(): String {
        return this.msg
    }
}