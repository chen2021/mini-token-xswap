package io.nchain.token.xswap.dict

/**
 * @author     ：CHEN
 * @date       ：Created in 2020/11/16 12:17
 * @description：
 * @modified By：
 * @version    ：1.0
 */
enum class RateRedisKey(val key: String, val expiredTs:Long?, val remark:String)  {
    WAYKIX_RATE("WAYKIX:RATE",1 * 60 * 60, "汇率"),
    WAYKIX_PRICE("WAYKIX:PRICE",null, "wicc/wgrt/rog价格"),
}