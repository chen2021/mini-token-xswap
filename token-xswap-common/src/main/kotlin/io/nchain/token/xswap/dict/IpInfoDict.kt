package io.nchain.token.xswap.dict

/**
 * @author     ：CHEN
 * @date       ：Created in 2020/12/31 16:24
 * @description：
 * @modified By：
 * @version    ：1.0
 */
enum class IpInfoDict(val msg:String,val description:String) {
    RET           ("ret","ok,error"),
    IP            ("ip",""),
    DATA          ("data","")
}