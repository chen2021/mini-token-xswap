package io.nchain.token.xswap.domain.bean

/**
 * @author     ：CHEN
 * @date       ：Created in 2020/11/19 11:09
 * @description：
 * @modified By：
 * @version    ：1.0
 */
class BalanceResult {
    var cryptype: Int = 0
    var data = BalanceResultData()

    class BalanceResultData {
        var eno: Int? = null
        var emsg: String? = null
        var data = ArrayList<SecDate>()

        class SecDate {
            var chain: String? = null
            var coin: String? = null
            var balance: String? = null
            var as_cny: String? = null
        }
    }
}