package io.nchain.token.xswap.valid

import java.lang.annotation.Documented
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import javax.validation.Constraint
import javax.validation.Payload
import kotlin.reflect.KClass

/**
 *
 * @author     : CHEN
 * @Date       : Created in 2021/3/13 11:12 下午
 * @description：
 * @modified By：
 * @Version    : 1.0
 **/

@Documented
@Constraint(validatedBy = [ListValueConstraintValidator::class])
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.FIELD, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CONSTRUCTOR, AnnotationTarget.VALUE_PARAMETER, AnnotationTarget.TYPE)
@Retention(RetentionPolicy.RUNTIME)
annotation class ListValue(val message: String = "必须提交指定的值（0或1）",
                           val groups: Array<KClass<*>> = [],
                           val payload: Array<KClass<out Payload>> = [],
                           val values: IntArray = [])

