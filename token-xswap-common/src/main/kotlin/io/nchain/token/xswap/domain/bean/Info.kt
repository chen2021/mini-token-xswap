package io.nchain.token.xswap.domain.bean

/**
 * @author     ：CHEN
 * @date       ：Created in 2020/11/19 12:23
 * @description：
 * @modified By：
 * @version    ：1.0
 */
class Info {
    var appid:String?=null
    var cryptype:Int=0
    var data=InfoData()
    class InfoData{
        var auth= DepositAddr.DepositData.Auth()
    }
}