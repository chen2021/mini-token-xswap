package io.nchain.token.xswap.domain.bean

/**
 * @author     ：CHEN
 * @date       ：Created in 2020/11/16 17:41
 * @description：
 * @modified By：
 * @version    ：1.0
 */
class WithdrawStatusResult {
    var cryptype: Int = 0
    var data = WithdrawStatusResultData()

    class WithdrawStatusResultData {
        var eno: Int? = null
        var emsg: String? = null
        var data = SecData()

        class SecData {
            var id: Int? = null
            var subuserid: String? = null
            var chain: String? = null
            var coin: String? = null
            var from_addr: String? = null
            var addr: String? = null
            var amount: String? = null
            var amount_sent: String? = null
            var sub_balance: String? = null
            var memo: String? = null
            var status: Int? = null
            var status_desc: String? = null
            var txid: String? = null
            var fee_coin: String? = null
            var fee_amount: String? = null
            var usertags: String? = null
            var time: String? = null
        }
    }
}

