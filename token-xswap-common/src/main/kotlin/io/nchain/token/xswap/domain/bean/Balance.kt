package io.nchain.token.xswap.domain.bean

/**
 * @author     ：CHEN
 * @date       ：Created in 2020/11/19 10:42
 * @description：
 * @modified By：
 * @version    ：1.0
 */
class Balance {
    var appid: String? = null
    var cryptype: Int = 0
    var data = BalanceData()

    class BalanceData {
        var auth = DepositAddr.DepositData.Auth()
        var coins = ArrayList<BalanceCoin>()

        class BalanceCoin {
            var chain: String? = null
            var coin: String? = null
        }
    }
}