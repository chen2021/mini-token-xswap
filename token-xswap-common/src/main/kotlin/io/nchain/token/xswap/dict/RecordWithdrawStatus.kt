package io.nchain.token.xswap.dict

/**
 * @author     ：CHEN
 * @date       ：Created in 2020/11/16 10:32
 * @description：
 * @modified By：
 * @version    ：1.0
 */
enum class RecordWithdrawStatus(val code: Int, val msg: String) {
    NO_SEND                 (0, "未提币"),
    READTY_TO_SEND          (1, "准备发送"),
    SENDING                 (2, "发送中"),
    SENT                    (3, "发送成功"),
    FAIL_TO_SEND            (4, "发送失败"),
    SENDING_CANCELED        (5, "发送已取消")
}