package io.nchain.token.xswap.domain.bean

/**
 *
 * @author     : CHEN
 * @Date       : Created in 2021/3/15 3:42 下午
 * @description：
 * @modified By：
 * @Version    : 1.0
 **/
class SdkInfo {
    var appId: String? = null
    var apiKey: String? = null
    var secretKey: String? = null
    var userId: String? = null
    var memo: String? = null
    var url: String? = null
}