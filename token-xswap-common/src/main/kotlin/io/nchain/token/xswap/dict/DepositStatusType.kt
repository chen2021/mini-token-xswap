package io.nchain.token.xswap.dict

/**
 * @author     ：CHEN
 * @date       ：Created in 2020/12/18 10:21
 * @description：
 * @modified By：
 * @version    ：1.0
 */
enum class DepositStatusType(val code: Int,val msg: String) {
    INVALIAD_STATUS(0,"无效状态"),
    NORMAL_STATUS(1,"正常入账"),
    WAITING_STATUS(2,"待入账"),

}