package io.nchain.token.xswap.dict

/**
 * @author     ：CHEN
 * @date       ：Created in 2020/11/14 15:05
 * @description：
 * @modified By：
 * @version    ：1.0
 */
object RequestType {
    val REQUEST_CUSTOMER_ID = "request_customer_id"
    val REQUEST_UUID_TAG = "request_uuid"
    val DEVICE_UUID_TAG = "device_uuid"
    val REQUEST_TIMESTAMP_TAG = "request_timestamp"
    val ACCESS_TOKENREQUEST_HEADER_TAG = "access_token"
    val ACTION_LOGIN_TOKEN_HEADER_TAG  = "action_login_token"
    val CHANNEL_CODE_HEADER_TAG  = "channel_code"
    var OAUTH_TOKEN_PREFIX_TYPE = "oauth2_token_pretype"
    var LANG_TAG = "lang" //多语言
    const val REQUEST_TIME_ZONE = "time_zone" //时区
    const val REQUEST_WALLET_ADDRESS = "wallet_address"
}