package io.nchain.token.xswap.domain.bean

/**
 * @author     ：CHEN
 * @date       ：Created in 2020/11/17 9:50
 * @description：提币预校验
 * @modified By：
 * @version    ：1.0
 */
class WithdrawValidator {
    var appid: String? = null
    var cryptype: Int = 0
    var data = WithdrawValidatorData()

    class WithdrawValidatorData {
        var auth = DepositAddr.DepositData.Auth()
        var subuserid: String? = null
        var chain: String? = null
        var coin: String? = null
        var addr: String? = null
        var amount: Float? = null
        var memo: String? = null
        var usertags: String? = null
        var sign: String? = null
    }
}