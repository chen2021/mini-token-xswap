package io.nchain.token.xswap.domain.po

import io.swagger.annotations.ApiModelProperty
import javax.validation.constraints.Min

/**
 * @author     ：CHEN
 * @date       ：Created in 2020/11/18 0:40
 * @description：
 * @modified By：
 * @version    ：1.0
 */
open class PageablePO {
    @ApiModelProperty(value = "当前页，从1开始（默认1）", example = "1")
    var currentpage: Int = 1

    @ApiModelProperty(value="每页条数（默认20）", example = "20")
    @Min(value=1,message = "[in param error] Minimum value of pageSize is 1")
    var pagesize: Int = 20

}