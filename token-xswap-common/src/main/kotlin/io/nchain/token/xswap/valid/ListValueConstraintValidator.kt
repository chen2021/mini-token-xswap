package io.nchain.token.xswap.valid

import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext

/**
 *
 * @author     : CHEN
 * @Date       : Created in 2021/3/13 11:14 下午
 * @description：
 * @modified By：
 * @Version    : 1.0
 **/
class ListValueConstraintValidator : ConstraintValidator<ListValue, Int> {
    val set = HashSet<Int>()
    override fun initialize(constraintAnnotation: ListValue) {
        for (it in constraintAnnotation.values) {
            set.add(it)
        }
    }

    override fun isValid(value: Int?, context: ConstraintValidatorContext?): Boolean {
        return set.contains(value)
    }
}