package io.nchain.token.xswap.utils

import java.net.InetAddress

/**
 * @author     ：CHEN
 * @date       ：Created in 2020/12/31 15:59
 * @description：
 * @modified By：
 * @version    ：1.0
 */
class IpUtil {
    companion object {


        /*
    * ip地址转int
    *
    * */
        fun bytesToInt(bytes: ByteArray): Int {
            var addr: Int = bytes[3].toInt() and 0xFF
            addr = addr or (bytes[2].toInt() shl 8 and 0xFF00)
            addr = addr or (bytes[1].toInt() shl 16 and 0xFF0000)
            addr = addr or (bytes[0].toInt() shl 24 and -0x1000000)
            return addr
        }

        /**
         * 把IP地址转化为字节数组
         * @param ipAddr
         * @return byte[]
         */
        fun ipToBytesByInet(ipAddr: String): ByteArray {
            return try {
                InetAddress.getByName(ipAddr).address
            } catch (e: Exception) {
                throw java.lang.IllegalArgumentException("$ipAddr is invalid IP")
            }
        }

        /**
         * 把IP地址转化为int
         * @param ipAddr
         * @return int
         */
        fun ipToInt(ipAddr: String): Int {
            return try {
                bytesToInt(ipToBytesByInet(ipAddr))
            } catch (e: Exception) {
                throw IllegalArgumentException("$ipAddr is invalid IP")
            }
        }

        /**
         * 把int->ip地址
         * @param ipInt
         * @return String
         */
        fun intToIp(ipInt: Int): String {
            return StringBuilder().append(ipInt shr 24 and 0xff).append('.')
                    .append(ipInt shr 16 and 0xff).append('.').append(
                            ipInt shr 8 and 0xff).append('.').append(ipInt and 0xff)
                    .toString()
        }

    }
}