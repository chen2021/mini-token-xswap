package io.nchain.token.xswap.dict

/**
 * @author     ：CHEN
 * @date       ：Created in 2020/11/14 15:03
 * @description：
 * @modified By：
 * @version    ：1.0
 */
enum class Language(val lang: String, val coinSymbol: String, val oldLang: String) {
    EN("EN", "usd", "en"),
    CN("CN", "cny", "zh-CHS");
}