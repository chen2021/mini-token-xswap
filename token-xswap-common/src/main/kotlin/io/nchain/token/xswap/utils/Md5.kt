package io.nchain.token.xswap.utils

import java.security.MessageDigest
import kotlin.experimental.and

/**
 * @author     ：CHEN
 * @date       ：Created in 2020/11/16 15:27
 * @description：
 * @modified By：
 * @version    ：1.0
 */
class Md5 {
    companion object {
        fun md5(tokenStr: String): String {
            val stringBuilder = StringBuilder()
            val md5s = MessageDigest.getInstance("MD5").digest(tokenStr.toByteArray(charset("utf-8")))
            md5s.forEach { stringBuilder.append(String.format("%02x", it and 0xff.toByte())) }
            return stringBuilder.toString()
        }

    }
}