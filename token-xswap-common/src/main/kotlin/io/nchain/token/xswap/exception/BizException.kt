package io.nchain.token.xswap.exception

import io.nchain.token.xswap.dict.ErrorCode
import java.lang.RuntimeException

class BizException(val code:Int,val msg:String):RuntimeException(msg){
    constructor(errorCode:ErrorCode):this(errorCode.code,errorCode.msg)
}