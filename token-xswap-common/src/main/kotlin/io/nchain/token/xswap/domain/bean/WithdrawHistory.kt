package io.nchain.token.xswap.domain.bean

/**
 * @author     ：CHEN
 * @date       ：Created in 2020/11/17 17:44
 * @description：提币记录
 * @modified By：
 * @version    ：1.0
 */
class WithdrawHistory {
    var appid: String? = null
    var cryptype: Int = 0
    var data = WithdrawHistoryData()

    class WithdrawHistoryData {
        var auth = DepositAddr.DepositData.Auth()
        var subuserid: String?=null
        var chain: String? = null
        var coin: String? = null
        var fromid: Int? = null
        var limit: Int? = null
    }
}