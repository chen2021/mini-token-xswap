package io.nchain.token.xswap.dict

enum class SysConfigName (val cfgName:String,val desp:String){
    APP_ID                              ("APP_ID","用户身份ID, 一个唯一的随机字符串"),
    API_KEY                             ("API_KEY",""),
    SECRETE_KEY                         ("SECRETE_KEY",""),
    UESR_ID                             ("USER_ID","商户ID"),
    MEMO                                ("MEMO","备注"),
    THIRD_PARTY_URL                     ("THIRD_PARTY_URL","第三方域名"),
    INSUFFICENT_WARNING_URL             ("INSUFFICENT_WARNING_URL","矿工费不足URL"),
    FAILED_PAY_WARNING_URL              ("FAILED_PAY_WARNING_URL","用户打币超时未到账"),
    DING_DEPOSIT_URL                    ("DING_DEPOSIT_URL","钉钉充币提醒URL"),
    DING_WITHDRAW_URL                   ("DING_WITHDRAW_URL","钉钉提币提醒URL"),
    RECORD_INDEX                        ("RECORD_INDEX","提币订单ID所在记录的ID"),
    WITHDRAW_SCAN_INDEX                 ("WITHDRAW_SCAN_INDEX","提币扫描数据库自增ID"),
    WITHDRAW_REQUEST_TIMES              ("WITHDRAW_REQUEST_TIMES","提币次数"),
    IP_URl                              ("IP_URL","解析Ip的url"),
    IP_TOKEN                            ("IP_TOKEN",""),
    MGP_DEPOSIT_ACCOUNT                 ("MGP_DEPOSIT_ACCOUNT","MGP充值账户")
}