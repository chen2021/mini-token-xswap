package io.nchain.token.xswap.domain.bean

/**
 * @author     ：CHEN
 * @date       ：Created in 2020/11/19 10:28
 * @description：
 * @modified By：
 * @version    ：1.0
 */
class CoinConf {
    var appid:String?=null
    var cryptype:Int=0
    var data= CoinConfPOData()
    class CoinConfPOData{
        var auth= DepositAddr.DepositData.Auth()
        var coin:String?=null
    }
}