package io.nchain.token.xswap.domain.bean

/**
 * @author     ：CHEN
 * @date       ：Created in 2020/11/16 15:13
 * @description：
 * @modified By：
 * @version    ：1.0
 */
class WithdrawSubmit {
    var appid: String? = null
    var cryptype: Int = 0
    var data = WithdrawSubmitData()

    class WithdrawSubmitData {
        var auth = DepositAddr.DepositData.Auth()
        var subuserid: String? = null
        var chain: String? = null
        var coin: String? = null
        var addr: String? = null
        var amount: Float? = null
        var memo: String? = null
        var usertags: String? = null
        var sign: String? = null
        var user_orderid: String? = null
    }
}