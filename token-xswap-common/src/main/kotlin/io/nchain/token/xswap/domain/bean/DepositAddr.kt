package io.nchain.token.xswap.domain.bean


class DepositAddr {
    var appid: String? = null
    var cryptype: Int = 0
    var data = DepositData()

    class DepositData {
        var auth = Auth()
        var coins = ArrayList<Coins>()


        class Auth {
            var token: String? = null
            var timestamp: Long = 0
            var api_key: String? = null
        }

        class Coins {
            var chain: String? = null
            var coin: String? = null
            var subuserid: String? = null
        }
    }
}