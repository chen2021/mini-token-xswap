package io.nchain.token.xswap.domain.bean

/**
 * @author     ：CHEN
 * @date       ：Created in 2020/11/17 9:55
 * @description：
 * @modified By：
 * @version    ：1.0
 */
class WithdrawValidatorResult {
    var cryptype: Int = 0
    var data = WithdrawValidatorResultData()

    class WithdrawValidatorResultData {
        var eno: Int? = null
        var emsg: String? = null
    }
}