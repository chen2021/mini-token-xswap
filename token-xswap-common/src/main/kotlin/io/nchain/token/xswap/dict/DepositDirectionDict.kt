package io.nchain.token.xswap.dict

/**
 *
 * @author     : CHEN
 * @Date       : Created in 2021/3/14 1:09 上午
 * @description：
 * @modified By：
 * @Version    : 1.0
 **/
enum class DepositDirectionDict(val code: Int, val msg: String) {
    DIRECT(0, "正向"),
    INDIRECT(1, "反向")
}