package io.nchain.token.xswap.utils

/**
 *
 * @author     : CHEN
 * @Date       : Created in 2021/3/15 2:35 上午
 * @description：
 * @modified By：
 * @Version    : 1.0
 **/
class GenerateRateNameUtils {
    companion object {
        fun generateRateNameUtils(depositChain: String, depositCoin: String, withdrawChain: String, withdrawCoin: String): String {
            return depositChain.toUpperCase() + "_" + depositCoin.toUpperCase() + ":" + withdrawChain.toUpperCase() + "_" + withdrawCoin.toUpperCase()
        }
    }
}