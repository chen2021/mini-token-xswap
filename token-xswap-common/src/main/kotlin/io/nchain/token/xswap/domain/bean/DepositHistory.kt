package io.nchain.token.xswap.domain.bean

class DepositHistory {
    var appid: String? = null
    var cryptype: Int = 0
    var data = DepositHistoryData()

    class DepositHistoryData {
        var auth = DepositAddr.DepositData.Auth()
        var subuserid: String? = null
        var chain: String? = null
        var coin: String? = null
        var fromid: Int? = null
        var limit: Int = 20
    }
}