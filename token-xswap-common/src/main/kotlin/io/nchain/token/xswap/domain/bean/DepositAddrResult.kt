package io.nchain.token.xswap.domain.bean

class DepositAddrResult {
    var cryptype: Int = 0
    lateinit var data: DepositData

    class DepositData {
        var eno: Int? = 0
        var emsg: String? = null
        lateinit var data: List<Data>

        class Data {
            var chain: String? = null
            var coin: String? = null
            var subuserid: String? = null
            var addr: String? = null
            var needmemo: Int = 1

        }
    }
}