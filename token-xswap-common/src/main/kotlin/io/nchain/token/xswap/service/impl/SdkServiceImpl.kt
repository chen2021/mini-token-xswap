package io.nchain.token.xswap.service.impl

import com.dingtalk.api.DefaultDingTalkClient
import com.dingtalk.api.DingTalkClient
import com.dingtalk.api.request.OapiRobotSendRequest
import com.waykichain.securitiesaccessories.commons.BizException
import io.nchain.token.xswap.dict.ErrorCode
import io.nchain.token.xswap.dict.SysConfigName
import io.nchain.token.xswap.domain.repository.SysConfigRepository
import io.nchain.token.xswap.service.SdkService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.DependsOn
import org.springframework.stereotype.Service
import pro.safeworld.swasdk.Sdk
import pro.safeworld.swasdk.User
import pro.safeworld.swasdk.data.Req.*
import pro.safeworld.swasdk.data.Resp.*
import javax.annotation.PostConstruct

/**
 *
 * @author     : CHEN
 * @Date       : Created in 2021/3/13 2:27 下午
 * @description：
 * @modified By：
 * @Version    : 1.0
 **/
@Service
//@DependsOn("entityManagerFactory")
@DependsOn("sysConfigRepository")
class SdkServiceImpl : SdkService {
    private var appId: String? = null
    private var apiKey: String? = null
    private var secretKey: String? = null
    private var userId: String? = null
    private var url: String? = null
    private lateinit var sdk: Sdk

    @PostConstruct
    fun init() {
        appId = sysConfigRepository.findByName(SysConfigName.APP_ID.cfgName).value
        apiKey = sysConfigRepository.findByName(SysConfigName.API_KEY.cfgName).value
        secretKey = sysConfigRepository.findByName(SysConfigName.SECRETE_KEY.cfgName).value
        userId = sysConfigRepository.findByName(SysConfigName.UESR_ID.cfgName).value
        url = sysConfigRepository.findByName(SysConfigName.THIRD_PARTY_URL.cfgName).value
        val user = User()
        user.appid = appId
        user.secretKey = secretKey
        user.userid = userId
        user.apiKey = apiKey
        sdk = Sdk(user)
        sdk.setHost(url)
    }

    override fun queryCoinConf(queryCoin: ReqQueryCoin): List<RespQueryCoinData> {
        val res = sdk.QueryCoinConf(queryCoin)
        if (res.emsg != "" || res.eno != 0) {
            throw BizException(ErrorCode.THIRTY_INTERFACE, res.eno, res.emsg)
        }
        return res.data
    }

    override fun queryBalance(balance: HashSet<ReqQueryBalanceBodyInfo>): List<RespQueryBalanceData> {
        val res = sdk.QueryBalance(balance)
        if (res.emsg != "" || res.eno != 0) {
            throw BizException(ErrorCode.THIRTY_INTERFACE, res.eno, res.emsg)
        }
        return res.data
    }

    override fun getDepositAddr(reqGetDepositAddrBodyInfoHashSet: HashSet<ReqGetDepositAddrBodyInfo>): List<RespGetDepositAddrData> {
        val res = sdk.GetDepositAddr(reqGetDepositAddrBodyInfoHashSet)
        if (res.emsg != "" || res.eno != 0) {
            throw BizException(ErrorCode.THIRTY_INTERFACE, res.eno, res.emsg)
        }
        return res.data
    }

    override fun getDepositHistory(reqGetDepositHistory: ReqGetDepositHistory): List<RespGetDepositHistoryData> {
        val res = sdk.GetDepositHistory(reqGetDepositHistory)
        if (res.emsg != "" || res.eno != 0) {
            throw BizException(ErrorCode.THIRTY_INTERFACE, res.eno, res.emsg)
        }
        return res.data
    }

    override fun submitWithdraw(reqSubmitWithdraw: ReqSubmitWithdraw): RespSubmitWithdrawBody {
        val res = sdk.SubmitWithdraw(reqSubmitWithdraw)
        if (res.emsg != "" || res.eno != 0) {
            logger.error("【withDrawJobHandler】===>[${res.eno}:${res.emsg}]")
            if (res.eno == 8) {
                dingWarning(res.emsg)
//                submitWithdraw(reqSubmitWithdraw)
                return res
            }
            throw BizException(ErrorCode.THIRTY_INTERFACE, res.eno, res.emsg)
        }
        logger.info("【withDrawJobHandler】:【提币请求】---提币订单ID：${res.data.id}-----提币数量：${res.data.amountSent}----提币交易哈希：${res.data.txid}")
        return res
    }

    override fun queryWithdrawStatus(reqQueryWithdrawStatus: ReqQueryWithdrawStatus): RespQueryWithdrawStatusBody {
        return sdk.QueryWithdrawStatus(reqQueryWithdrawStatus)
    }

    private fun dingWarning(msg: String) {
        val url = sysConfigRepository.findByName(SysConfigName.INSUFFICENT_WARNING_URL.cfgName).value
        val client: DingTalkClient = DefaultDingTalkClient(url)
        val request = OapiRobotSendRequest()

        request.msgtype = "text"
        val text = OapiRobotSendRequest.Text()
        text.content = "Warning:${msg}"
        request.setText(text)
        try {
            client.execute(request)
        } catch (e: Exception) {
        }
    }

    private val logger = LoggerFactory.getLogger(javaClass)

    @Autowired
    lateinit var sysConfigRepository: SysConfigRepository
}