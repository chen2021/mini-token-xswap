package io.nchain.token.xswap.domain.bean

class DepositHistoryResult {
    var cryptype: Int? = null
    var data = DepositHistoryResultData()

    class DepositHistoryResultData {
        var eno: Int? = null
        var emsg: String? = null
        var data = arrayListOf<res>()

        class res {
            var id: Int? = null
            var subuserid: String? = null
            var chain: String? = null
            var coin: String? = null
            var from: String? = null
            var addr: String? = null
            var txid: String? = null
            var amount: String? = null
            var balance: String? = null
            var time: String? = null
        }
    }

}