package io.nchain.token.xswap.domain.vo

/**
 * @author     ：CHEN
 * @date       ：Created in 2020/11/16 1:18
 * @description：
 * @modified By：
 * @version    ：1.0
 */
open class CallBackResponse<T> (var cryptype: Int, var data: T?) {

    constructor(data: T?) : this(0, data)
}

