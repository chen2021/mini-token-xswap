package io.nchain.token.xswap.domain.bean

/**
 * @author     ：CHEN
 * @date       ：Created in 2020/11/19 10:30
 * @description：
 * @modified By：
 * @version    ：1.0
 */
class CoinConfResult {
    var cryptype: Int = 0
    var data = CoinConfVOData()

    class CoinConfVOData {
        var eno: Int? = null
        var emsg: String? = null
        var data = ArrayList<CoinConfVOData>()

        class CoinConfVOData {
            var chain: String? = null
            var coin: String? = null
            var coin_precision: Int? = null
            var min_deposit_amount: String? = null
            var min_withdraw_amount: String? = null
            var deposit_enabled: Int? = null
            var withdraw_enabled: Int? = null
            var deposit_confirm_count: Int? = null
            var fee_coin: String? = null
            var fee_type: Int? = null
            var fee_amount: String? = null
            var fee_rate: String? = null
            var need_memo: Int? = null
        }
    }
}