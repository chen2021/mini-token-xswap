package io.nchain.token.xswap.service

import pro.safeworld.swasdk.data.Req.*
import pro.safeworld.swasdk.data.Resp.*

/**
 *
 * @author     : CHEN
 * @Date       : Created in 2021/3/13 2:24 下午
 * @description：
 * @modified By：
 * @Version    : 1.0
 **/
interface SdkService {
    //单个币种查询
    fun queryCoinConf(queryCoin: ReqQueryCoin): List<RespQueryCoinData>

    //查询余额
    fun queryBalance(balance: HashSet<ReqQueryBalanceBodyInfo>): List<RespQueryBalanceData>

    //获取充值地址
    fun getDepositAddr(reqGetDepositAddrBodyInfoHashSet: HashSet<ReqGetDepositAddrBodyInfo>): List<RespGetDepositAddrData>

    //获取充值记录
    fun getDepositHistory(reqGetDepositHistory: ReqGetDepositHistory): List<RespGetDepositHistoryData>

    //提交提币工单
    fun submitWithdraw(reqSubmitWithdraw: ReqSubmitWithdraw): RespSubmitWithdrawBody

    //查询工单状态
    fun queryWithdrawStatus(reqQueryWithdrawStatus: ReqQueryWithdrawStatus): RespQueryWithdrawStatusBody
}