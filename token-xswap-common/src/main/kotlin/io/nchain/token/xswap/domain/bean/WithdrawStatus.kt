package io.nchain.token.xswap.domain.bean

/**
 * @author     ：CHEN
 * @date       ：Created in 2020/11/16 17:41
 * @description： 查询提币工单状态
 * @modified By：
 * @version    ：1.0
 */
class WithdrawStatus {
    var appid: String? = null
    var cryptype: Int = 0
    var data = WithdrawStatusData()

    class WithdrawStatusData {
        var auth = DepositAddr.DepositData.Auth()
        var chain: String? = null
        var coin: String? = null
        var withdrawid: Int? = null
    }

}