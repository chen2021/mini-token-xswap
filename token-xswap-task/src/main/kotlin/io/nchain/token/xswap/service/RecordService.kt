package io.nchain.token.xswap.service


import io.nchain.token.xswap.domain.entity.ThirdCallbackLog

/**
 * @author     ：CHEN
 * @date       ：Created in 2020/11/16 10:46
 * @description：
 * @modified By：
 * @version    ：1.0
 */
interface RecordService {
    fun save(thirdCallbackLog: ThirdCallbackLog)

    fun queryRecord(index: Long): List<ThirdCallbackLog>?
}