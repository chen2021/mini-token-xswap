package io.nchain.token.xswap.domain.bean

import io.swagger.annotations.ApiModelProperty
import javax.validation.constraints.Min

/**
 *
 * @author     : CHEN
 * @Date       : Created in 2021/4/2 5:06 下午
 * @description：
 * @modified By：
 * @Version    : 1.0
 **/
class RecordListBean : BasePageablePO() {
    var status: Int? = null

    var id: Long = 0
}

open class BasePageablePO {

    var currentpage: Int = 1

    var pagesize: Int = 20
}
