package io.nchain.token.xswap.config

import com.waykichain.securitiesaccessories.taskconfig.TaskExecutorConfig
import org.springframework.context.annotation.Configuration

/**
 * @author     ：CHEN
 * @date       ：Created in 2020/11/16 9:45
 * @description：
 * @modified By：
 * @version    ：1.0
 */
@Configuration
open class TokenTaskExecutorConfig : TaskExecutorConfig()