package io.nchain.token.xswap

import com.waykichain.securitiesaccessories.utils.EnvironmentInit
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cache.annotation.EnableCaching

/**
 * @author     ：CHEN
 * @date       ：Created in 2021/1/31 14:02
 * @description：
 * @modified By：
 * @version    ：1.0
 */
@SpringBootApplication
@EnableCaching
open class Application
fun main(args:Array<String>){

    EnvironmentInit.initTimeZone(0)
    runApplication<Application>(*args)
}