package io.nchain.token.xswap.jobs

import com.xxl.job.core.biz.model.ReturnT
import com.xxl.job.core.handler.annotation.XxlJob
import io.nchain.token.xswap.dict.DepositStatusType
import io.nchain.token.xswap.dict.RecordWithdrawStatus
import io.nchain.token.xswap.dict.SysConfigName
import io.nchain.token.xswap.domain.bean.RecordListBean
import io.nchain.token.xswap.domain.entity.QThirdCallbackLog
import io.nchain.token.xswap.domain.entity.ThirdCallbackLog
import io.nchain.token.xswap.domain.repository.SysConfigRepository
import io.nchain.token.xswap.domain.repository.ThirdCallbackLogRepository
import io.nchain.token.xswap.service.RecordService
import io.nchain.token.xswap.service.SdkService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import pro.safeworld.swasdk.data.Req.ReqSubmitWithdraw
import pro.safeworld.swasdk.data.Resp.RespSubmitWithdrawBody
import pro.safeworld.swasdk.data.Resp.RespSubmitWithdrawData
import java.math.BigDecimal
import java.text.SimpleDateFormat
import javax.annotation.PostConstruct

/**
 * @author     ：CHEN
 * @date       ：Created in 2020/11/16 9:40
 * @description：
 * @modified By：
 * @version    ：1.0
 */

@Service
class WithdrawJobHandler {
    private var memo: String? = null

    @PostConstruct
    fun init() {
        memo = sysConfigRepository.findByName(SysConfigName.MEMO.cfgName).value
    }

    @XxlJob("withdrawJobHandler")
    fun withdrawJobHandler(): ReturnT<String> {
        val indexEntity = sysConfigRepository.findByName(SysConfigName.WITHDRAW_SCAN_INDEX.cfgName)
        val po = RecordListBean()
        while (true) {
            po.status = 0
            val result = recordService.queryRecord(indexEntity.value.toLong())
            if (result!!.isEmpty()) break
            handleRecord(result)
            indexEntity.value = (result[result.size - 1].id + 1).toString()
            sysConfigRepository.save(indexEntity)
        }
        return ReturnT.SUCCESS
    }

    private fun handleRecord(result: List<ThirdCallbackLog>) {
        result.filter {
            it.depositStatus == DepositStatusType.WAITING_STATUS.code
        }.forEach {
            var res = thirdCallbackLogRepository.findOne(QThirdCallbackLog.qThirdCallbackLog.id.eq(it.id)).get()
            if (res.depositStatus == DepositStatusType.NORMAL_STATUS.code) {
                executeWithdraw(res)
            }
            while (res.depositStatus == DepositStatusType.WAITING_STATUS.code) {
                res = thirdCallbackLogRepository.findOne(QThirdCallbackLog.qThirdCallbackLog.id.eq(it.id)).get()
                if (res.depositStatus == DepositStatusType.NORMAL_STATUS.code) {
                    executeWithdraw(res)
                    break
                }
            }
        }

        result.filter {
            it.depositStatus == DepositStatusType.NORMAL_STATUS.code && (it.withdrawOrderId == -1L || it.withdrawStatus == RecordWithdrawStatus.FAIL_TO_SEND.code)
        }.forEach { record ->
            executeWithdraw(record)
        }
    }

    private fun executeWithdraw(record: ThirdCallbackLog) {
        var submitReqTimes = 1
        //根据汇率 计算WUSD数量
        val exchangeRate = record.exchangeRate
        val gas = record.gas
        val exchangeFeeAmount = record.depositAmount.multiply(BigDecimal.ONE - exchangeRate)
        val amount = record.depositAmount.subtract(BigDecimal(gas.toString())).subtract(BigDecimal(exchangeFeeAmount.toString()))
        val reqSubmitWithdraw = ReqSubmitWithdraw()
        reqSubmitWithdraw.addr = record.withdrawToAddr
        reqSubmitWithdraw.amount = amount.toPlainString()
        reqSubmitWithdraw.chain = record.withdrawChain
        reqSubmitWithdraw.coin = record.withdrawCoinSymbol
        reqSubmitWithdraw.memo = memo!!.replace("%", record.id.toString())
        reqSubmitWithdraw.subuserid = record.walletAddr
        reqSubmitWithdraw.userOrderid = record.walletAddr + "_" + record.id
        logger.info("【发起提币请求】：数据库id---${record.id} | walletAddr---${record.walletAddr} ")
        var withdrawResultBody = sdkService.submitWithdraw(reqSubmitWithdraw)
        if (withdrawResultBody.eno == 0) {
            dealResult(withdrawResultBody, submitReqTimes, record, reqSubmitWithdraw, exchangeFeeAmount)
        } else {
            while (withdrawResultBody.eno == 8) {
                submitReqTimes += 1
                withdrawResultBody = sdkService.submitWithdraw(reqSubmitWithdraw)
                if (submitReqTimes == 10) {
                    val sysConfigEntity = sysConfigRepository.findByName(SysConfigName.WITHDRAW_REQUEST_TIMES.cfgName)
                    sysConfigEntity.value = record.id.toString() + ":" + submitReqTimes
                    sysConfigRepository.saveAndFlush(sysConfigEntity)
                    break
                }
            }
            if (withdrawResultBody.eno == 0) {
                dealResult(withdrawResultBody, 1, record, reqSubmitWithdraw, exchangeFeeAmount)
            }
        }
    }

    private fun dealResult(withdrawResultBody: RespSubmitWithdrawBody, submitReqTime: Int, record: ThirdCallbackLog, reqSubmitWithdraw: ReqSubmitWithdraw, exchangeFeeAmount: BigDecimal) {
        var submitReqTimes = submitReqTime
        var withdrawResult: RespSubmitWithdrawData
        withdrawResult = withdrawResultBody.data
        logger.info("【提币响应状态】----》${withdrawResult.status}|【提币订单id】----》${withdrawResult.id}")
        while (withdrawResult.status == RecordWithdrawStatus.FAIL_TO_SEND.code) {
            submitReqTimes += 1
            withdrawResult = sdkService.submitWithdraw(reqSubmitWithdraw).data
            if (submitReqTimes == 10) {
                val sysConfigEntity = sysConfigRepository.findByName(SysConfigName.WITHDRAW_REQUEST_TIMES.cfgName)
                sysConfigEntity.value = record.id.toString() + ":" + submitReqTimes
                sysConfigRepository.saveAndFlush(sysConfigEntity)
                break
            }
        }
        record.withdrawOrderId = withdrawResult.id
        record.withdrawAmount = withdrawResult.amountSent?.toBigDecimal() ?: BigDecimal.ONE
        record.withdrawAmountSent = withdrawResult.amountSent?.toBigDecimal() ?: BigDecimal.ONE
        record.depositMemo = withdrawResult.memo
        record.feeCoin = withdrawResult.feeCoin
        record.thirdFeeAmount = withdrawResult.feeAmount
        record.withdrawOrderCreatedAt = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(withdrawResult.time)
        record.withdrawStatus = withdrawResult.status
        record.statusDesc = withdrawResult.statusDesc
        record.ownFeeAmount = exchangeFeeAmount.toString()
        recordService.save(record)

    }

    private val logger = LoggerFactory.getLogger(javaClass)

    @Autowired
    lateinit var recordService: RecordService

    @Autowired
    lateinit var sysConfigRepository: SysConfigRepository

    @Autowired
    lateinit var sdkService: SdkService

    @Autowired
    lateinit var thirdCallbackLogRepository: ThirdCallbackLogRepository
}