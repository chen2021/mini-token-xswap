package io.nchain.token.xswap.service.impl


import io.nchain.token.xswap.domain.entity.ThirdCallbackLog
import io.nchain.token.xswap.domain.repository.ThirdCallbackLogRepository
import io.nchain.token.xswap.service.RecordService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * @author     ：CHEN
 * @date       ：Created in 2020/11/16 10:47
 * @description：
 * @modified By：
 * @version    ：1.0
 */
@Service
class RecordServiceImpl : RecordService {

    override fun save(thirdCallbackLog: ThirdCallbackLog) {
        thirdCallbackLogRepository.saveAndFlush(thirdCallbackLog)
    }

    override fun queryRecord(index: Long): List<ThirdCallbackLog>? {
        return thirdCallbackLogRepository.select(index, 20)
    }

    @Autowired
    lateinit var thirdCallbackLogRepository: ThirdCallbackLogRepository
}