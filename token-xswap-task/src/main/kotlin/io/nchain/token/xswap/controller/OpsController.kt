package io.nchain.token.xswap.controller

import com.waykichain.securitiesaccessories.commons.BizResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

/**
 * @author     ：CHEN
 * @date       ：Created in 2021/1/31 13:59
 * @description：
 * @modified By：
 * @version    ：1.0
 */
@RestController
@RequestMapping("ops")
class OpsController {
    @GetMapping("ver")
    fun getVersion():BizResponse<String> {
        return BizResponse("1.0.1")
    }
}