package io.nchain.token.xswap.domain.vo

import io.swagger.annotations.ApiModelProperty
import java.math.BigDecimal

/**
 * @author     ：CHEN
 * @date       ：Created in 2020/11/17 15:00
 * @description：
 * @modified By：
 * @version    ：1.0
 */
class RecordVO {
    var list = arrayListOf<RecordList>()

    var totalPages: Int = 0

    var currentPage: Int = 1

    class RecordList {
        @ApiModelProperty("数据库id")
        var id: Long? = null

        @ApiModelProperty("充币类型")
        var depositCoinSymbol: String = "ERC20_USDT"

        @ApiModelProperty("提币类型 ")
        var withdrawCoinSymbol: String? = null

        @ApiModelProperty("钱包地址")
        var walletAddr: String? = null

        @ApiModelProperty("充币数量")
        var depositAmount: BigDecimal? = null

        @ApiModelProperty("提币数量")
        var withdrawAmountSent: BigDecimal? = null

        @ApiModelProperty("状态：兑换成功 、待确认")
        var statusDesc: String? = null

        @ApiModelProperty("createdAt时间")
        var createAt: Long? = null

    }
}