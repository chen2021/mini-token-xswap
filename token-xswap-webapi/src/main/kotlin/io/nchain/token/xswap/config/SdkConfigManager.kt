package io.nchain.token.xswap.config

import io.nchain.token.xswap.dict.SysConfigName
import io.nchain.token.xswap.domain.bean.SdkInfo
import io.nchain.token.xswap.domain.repository.SysConfigRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.DependsOn
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct

/**
 *
 * @author     : CHEN
 * @Date       : Created in 2021/3/15 3:36 下午
 * @description：
 * @modified By：
 * @Version    : 1.0
 **/
@Component
//@DependsOn("entityManagerFactory")
@DependsOn("sysConfigRepository")
class SdkConfigManager {
    private var appId: String? = null
    private var apiKey: String? = null
    private var secretKey: String? = null
    private var userId: String? = null
    private var memo: String? = null
    private var url: String? = null
    @Autowired
    lateinit var sysConfigRepository: SysConfigRepository

    @PostConstruct
    fun init() {
        appId = sysConfigRepository.findByName(SysConfigName.APP_ID.cfgName).value
        apiKey = sysConfigRepository.findByName(SysConfigName.API_KEY.cfgName).value
        secretKey = sysConfigRepository.findByName(SysConfigName.SECRETE_KEY.cfgName).value
        userId = sysConfigRepository.findByName(SysConfigName.UESR_ID.cfgName).value
        memo = sysConfigRepository.findByName(SysConfigName.MEMO.cfgName).value
        url = sysConfigRepository.findByName(SysConfigName.THIRD_PARTY_URL.cfgName).value
    }
    fun getSdkInfo(): SdkInfo {
        val vo = SdkInfo()
        vo.appId = appId
        vo.apiKey = apiKey
        vo.secretKey = secretKey
        vo.userId = userId
        vo.memo = memo
        vo.url = url
        return vo
    }
}