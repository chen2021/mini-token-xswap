package io.nchain.token.xswap.service.impl

import com.waykichain.securitiesaccessories.commons.BizException
import io.nchain.token.xswap.dict.DepositDirectionDict
import io.nchain.token.xswap.dict.ErrorCode
import io.nchain.token.xswap.domain.entity.*
import io.nchain.token.xswap.domain.po.DepositAddrPO
import io.nchain.token.xswap.domain.po.QuickSwapPO
import io.nchain.token.xswap.domain.repository.DepositAddrRepository
import io.nchain.token.xswap.domain.repository.ExchangeRateRepository
import io.nchain.token.xswap.domain.repository.SysConfigRepository
import io.nchain.token.xswap.domain.vo.DepositAddrVO
import io.nchain.token.xswap.service.DepositService
import io.nchain.token.xswap.service.SdkService
import io.nchain.token.xswap.utils.GenerateRateNameUtils
import io.nchain.token.xswap.utils.MgpStringUtils
import org.slf4j.LoggerFactory
import org.springframework.beans.BeanUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import pro.safeworld.swasdk.data.Req.ReqGetDepositAddrBodyInfo

/**
 *
 * @author     : CHEN
 * @Date       : Created in 2021/3/13 1:55 下午
 * @description：
 * @modified By：
 * @Version    : 1.0
 **/
@Service
class DepositServiceImpl : DepositService {
    override fun getDepositAddr(po: DepositAddrPO, ip: String?): DepositAddrVO {
        val vo = DepositAddrVO()
        if (po.direction == DepositDirectionDict.DIRECT.code) {
            //正向 ERC20、TRC20---》MGP
            //数据库存在地址 直接返回
            val res = depositAddrRepository.findOne(QDepositAddr.qDepositAddr.walletAddr.eq(po.walletAddr)
                    .and(QDepositAddr.qDepositAddr.direction.eq(po.direction))
                    .and(QDepositAddr.qDepositAddr.thirdChain.eq(po.thirdChain))
                    .and(QDepositAddr.qDepositAddr.thirdCoinSymbol.eq(po.thirdCoinSymbol))
                    .and(QDepositAddr.qDepositAddr.ownCoinSymbol.eq(po.ownCoinSymbol)))
            val rateName = GenerateRateNameUtils.generateRateNameUtils(po.thirdChain!!, po.thirdCoinSymbol!!, po.ownChain!!, po.ownCoinSymbol!!)
            val rate = exchangeRateRepository.findByName(rateName).exchageRate.toString()
            if (res.isPresent) {
                vo.addr = res.get().thirdAddr
                vo.rate = rate
                return vo
            }
            //申请地址
            val set = HashSet<ReqGetDepositAddrBodyInfo>()
            val reqGetDepositAddrBodyInfo = ReqGetDepositAddrBodyInfo()
            reqGetDepositAddrBodyInfo.chain = po.thirdChain
            reqGetDepositAddrBodyInfo.coin = po.thirdCoinSymbol
            reqGetDepositAddrBodyInfo.subuserid = po.walletAddr
            set.add(reqGetDepositAddrBodyInfo)
            val result = sdkService.getDepositAddr(set)
            vo.addr = result.first().addr
            vo.rate = rate
            //钱包地址和充值地址存DB
            val depositAddr = DepositAddr()
            BeanUtils.copyProperties(po, depositAddr)
            depositAddr.thirdAddr = vo.addr
            depositAddr.ip = ip
            depositAddrRepository.saveAndFlush(depositAddr)
            return vo
        } else {
            //申请地址
            val set = HashSet<ReqGetDepositAddrBodyInfo>()
            val reqGetDepositAddrBodyInfo = ReqGetDepositAddrBodyInfo()
            reqGetDepositAddrBodyInfo.chain = po.ownChain
            reqGetDepositAddrBodyInfo.coin = po.ownCoinSymbol
            reqGetDepositAddrBodyInfo.subuserid = po.walletAddr + System.currentTimeMillis().toString()
            set.add(reqGetDepositAddrBodyInfo)
            val result = sdkService.getDepositAddr(set)
            //反向 MGP---》ERC20、TRC20
            val rateName = GenerateRateNameUtils.generateRateNameUtils(po.ownChain!!, po.ownCoinSymbol!!, po.thirdChain!!, po.thirdCoinSymbol!!)
            vo.addr = result.first().addr
            vo.rate = exchangeRateRepository.findByName(rateName).exchageRate.toString()
            vo.depositMemo = result.first().memo
            return vo
        }

    }

    override fun quickSwap(po: QuickSwapPO, ip: String?) {
        val depositMemo = MgpStringUtils.subString(po.depositMemo, "#", "#")
        if (po.direction == DepositDirectionDict.DIRECT.code) throw BizException(ErrorCode.WRONG_DIRECITION)
        if (depositAddrRepository.findOne(QDepositAddr.qDepositAddr.walletAddr.eq(po.walletAddr)
                        .and(QDepositAddr.qDepositAddr.depositMemo.eq(depositMemo))
                        .and(QDepositAddr.qDepositAddr.thirdAddr.eq(po.thirdAddr))).isPresent)
            throw BizException(ErrorCode.QUICK_SWAP)
        val depositAddr = DepositAddr()
        BeanUtils.copyProperties(po, depositAddr)
        depositAddr.depositMemo = depositMemo
        depositAddr.ip = ip
        depositAddrRepository.saveAndFlush(depositAddr)
    }

    private val log = LoggerFactory.getLogger(DepositServiceImpl::class.java)

    @Autowired
    lateinit var depositAddrRepository: DepositAddrRepository

    @Autowired
    lateinit var sysConfigRepository: SysConfigRepository

    @Autowired
    lateinit var exchangeRateRepository: ExchangeRateRepository

    @Autowired
    lateinit var sdkService: SdkService
}