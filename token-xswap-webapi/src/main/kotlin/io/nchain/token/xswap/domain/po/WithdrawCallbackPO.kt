package io.nchain.token.xswap.domain.po

import io.nchain.token.xswap.domain.bean.DepositAddr


/**
 * @author     ：CHEN
 * @date       ：Created in 2020/11/16 0:10
 * @description：
 * @modified By：
 * @version    ：1.0
 */
class WithdrawCallbackPO {
    var appid: String? = null
    var cryptype: Int = 0
    var data = WithdrawCallbackPOData()

    class WithdrawCallbackPOData {
        var auth = DepositAddr.DepositData.Auth()
        var id: Int? = null
        var subuserid: String? = null
        var chain: String? = null
        var coin: String? = null
        var from_addr: String? = null
        var addr: String? = null
        var amount: String? = null
        var amount_sent: String? = null
        var sub_balance: String? = null
        var memo: String? = null
        var status: Int? = null   //提币状态: 0=无效状态,1=准备发送,2=发送中,3=发送成功,4=发送失败,5=待确认
        var status_desc: String? = null
        var origin_txid: String? = null   //源交易 ID（加速情况下的第一笔交易 ID）
        var txid: String? = null
        var fee_coin: String? = null
        var fee_amount: String? = null
        var usertags: String? = null
        var user_orderid: String? = null  //用户系统流水号 ID
        var height: String? = null
        var time: String? = null
    }
}