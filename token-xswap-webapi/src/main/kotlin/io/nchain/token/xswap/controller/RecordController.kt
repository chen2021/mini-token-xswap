package io.nchain.token.xswap.controller

import com.waykichain.securitiesaccessories.commons.BizResponse
import io.nchain.token.xswap.domain.po.DepositAddrPO
import io.nchain.token.xswap.domain.po.RecordDetailPO
import io.nchain.token.xswap.domain.vo.RecordDetailVO
import io.nchain.token.xswap.domain.vo.RecordVO
import io.nchain.token.xswap.service.RecordService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

/**
 *
 * @author     : CHEN
 * @Date       : Created in 2021/3/17 4:41 下午
 * @description：
 * @modified By：
 * @Version    : 1.0
 **/
@Api(description = "兑换记录相关")
@RestController
@RequestMapping("record")
class RecordController {

    @PostMapping("list")
    @ApiOperation("获取兑换记录", httpMethod = "POST", notes = "获取兑换记录",produces = MediaType.APPLICATION_JSON_VALUE)
    fun getListRecord(@Valid @RequestBody po: DepositAddrPO): BizResponse<RecordVO> {
        return BizResponse(recordService.getList(po))
    }

    @PostMapping("detail")
    @ApiOperation("获取记录详情", httpMethod = "POST",notes = "获取记录详情", produces = MediaType.APPLICATION_JSON_VALUE)
    fun getDetail(@RequestBody po: RecordDetailPO): BizResponse<RecordDetailVO> {
        return BizResponse(recordService.getDetail(po))
    }
    @Autowired
    lateinit var recordService: RecordService
}