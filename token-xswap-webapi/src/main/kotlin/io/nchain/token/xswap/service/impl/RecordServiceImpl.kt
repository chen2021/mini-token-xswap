package io.nchain.token.xswap.service.impl

import io.nchain.token.xswap.dict.DepositDirectionDict
import io.nchain.token.xswap.dict.RecordWithdrawStatus
import io.nchain.token.xswap.domain.entity.QThirdCallbackLog
import io.nchain.token.xswap.domain.entity.ThirdCallbackLog
import io.nchain.token.xswap.domain.po.DepositAddrPO
import io.nchain.token.xswap.domain.po.RecordDetailPO
import io.nchain.token.xswap.domain.repository.ThirdCallbackLogRepository
import io.nchain.token.xswap.domain.vo.RecordDetailVO
import io.nchain.token.xswap.domain.vo.RecordVO
import io.nchain.token.xswap.service.RecordService
import io.nchain.token.xswap.service.SwapService
import io.nchain.token.xswap.utils.GenerateRateNameUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service

/**
 * @author     ：CHEN
 * @date       ：Created in 2020/11/16 10:47
 * @description：
 * @modified By：
 * @version    ：1.0
 */
@Service
class RecordServiceImpl : RecordService {

    override fun save(thirdCallbackLog: ThirdCallbackLog) {
        thirdCallbackLogRepository.saveAndFlush(thirdCallbackLog)
    }

    override fun getList(po: DepositAddrPO): RecordVO {
        val vo = RecordVO()
        val sort = Sort.by(Sort.Direction.DESC, "updatedAt")
        val pageRequest = PageRequest.of(po.currentpage - 1, po.pagesize, sort)
        var predicate = QThirdCallbackLog.qThirdCallbackLog.walletAddr.eq(po.walletAddr)
                .and(QThirdCallbackLog.qThirdCallbackLog.direction.eq(po.direction))
                .and(QThirdCallbackLog.qThirdCallbackLog.withdrawStatus.eq(RecordWithdrawStatus.SENT.code)
                        .or(QThirdCallbackLog.qThirdCallbackLog.withdrawStatus.eq(RecordWithdrawStatus.READTY_TO_SEND.code))
                        .or(QThirdCallbackLog.qThirdCallbackLog.withdrawStatus.eq(RecordWithdrawStatus.SENDING.code)))
        predicate = if (po.direction == DepositDirectionDict.DIRECT.code) {
            predicate.and(QThirdCallbackLog.qThirdCallbackLog.depositChain.eq(po.thirdChain))
                    .and(QThirdCallbackLog.qThirdCallbackLog.depositCoinSymbol.eq(po.thirdCoinSymbol))
                    .and(QThirdCallbackLog.qThirdCallbackLog.withdrawChain.eq(po.ownChain))
                    .and(QThirdCallbackLog.qThirdCallbackLog.withdrawCoinSymbol.eq(po.ownCoinSymbol))
        } else {
            predicate.and(QThirdCallbackLog.qThirdCallbackLog.depositChain.eq(po.ownChain))
                    .and(QThirdCallbackLog.qThirdCallbackLog.depositCoinSymbol.eq(po.ownCoinSymbol))
                    .and(QThirdCallbackLog.qThirdCallbackLog.withdrawChain.eq(po.thirdChain))
                    .and(QThirdCallbackLog.qThirdCallbackLog.withdrawCoinSymbol.eq(po.thirdCoinSymbol))
        }
        val records = thirdCallbackLogRepository.findAll(predicate, pageRequest)
        vo.totalPages = records.totalPages
        vo.currentPage = po.currentpage
        records.forEach {
            val flag = it.direction.toString() + ":" + GenerateRateNameUtils.generateRateNameUtils(it.depositChain, it.depositCoinSymbol, it.withdrawChain, it.withdrawCoinSymbol)
            val pairCoinSymbol = swapService.getPairCoinSymbol(flag)

            val detail = RecordVO.RecordList()
            detail.id = it.id
            detail.walletAddr = it.walletAddr
            detail.depositAmount = it.depositAmount
            detail.withdrawAmountSent = it.withdrawAmountSent
            detail.depositCoinSymbol = pairCoinSymbol.first
            detail.withdrawCoinSymbol = pairCoinSymbol.second
            detail.statusDesc = if (it.withdrawStatus == RecordWithdrawStatus.SENT.code) "兑换成功" else " 待确认"
            detail.createAt = if (it.withdrawStatus == RecordWithdrawStatus.SENT.code) it.withdrawOrderCreatedAt.time
            else it.depositConfirmedAt.time
            vo.list.add(detail)
        }
        return vo
    }

    override fun getDetail(po: RecordDetailPO): RecordDetailVO {
        val record = thirdCallbackLogRepository.findOne(QThirdCallbackLog.qThirdCallbackLog.id.eq(po.id))
        val detail = RecordDetailVO()
        record.get().let {
            val flag = it.direction.toString() + ":" + GenerateRateNameUtils.generateRateNameUtils(it.depositChain, it.depositCoinSymbol, it.withdrawChain, it.withdrawCoinSymbol)
            val pairCoinSymbol = swapService.getPairCoinSymbol(flag)

            detail.id = it.id
            detail.walletAddr = it.walletAddr
            detail.depositFromAddr = it.depositFromAddr
            detail.withdrawToAddr = it.withdrawToAddr
            detail.depositTxId = it.depositTxid
            detail.depositAmount = it.depositAmount
            detail.depositConfirmTime = it.depositConfirmedAt.time
            detail.withdrawAmountSent = it.withdrawAmountSent
            detail.withdrawTxId = it.withdrawTxid
            detail.depositCoinSymbol = pairCoinSymbol.first
            detail.withdrawCoinSymbol = pairCoinSymbol.second
            detail.statusDesc = if (it.withdrawStatus == RecordWithdrawStatus.SENT.code) "兑换成功" else " 待确认"
            detail.withdrawConfirmTime = it.updatedAt.time
        }
        return detail
    }

    @Autowired
    lateinit var thirdCallbackLogRepository: ThirdCallbackLogRepository

    @Autowired
    lateinit var swapService: SwapService
}