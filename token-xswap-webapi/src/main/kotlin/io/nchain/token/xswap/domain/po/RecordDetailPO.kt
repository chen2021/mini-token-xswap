package io.nchain.token.xswap.domain.po

import io.swagger.annotations.ApiModelProperty

/**
 * @author     ：CHEN
 * @date       ：Created in 2020/11/18 9:57
 * @description：
 * @modified By：
 * @version    ：1.0
 */
class RecordDetailPO{
    @ApiModelProperty("cdp 数据库id")
    var id: Long? = null
}