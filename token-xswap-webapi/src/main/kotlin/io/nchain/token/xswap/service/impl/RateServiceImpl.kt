package io.nchain.token.xswap.service.impl

import io.nchain.token.xswap.domain.repository.SysConfigRepository
import io.nchain.token.xswap.service.RateService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.math.BigDecimal

/**
 * @author     ：CHEN
 * @date       ：Created in 2020/11/16 11:22
 * @description：
 * @modified By：
 * @version    ：1.0
 */
@Service
class RateServiceImpl:RateService {
    override fun queryRate(thirdCoinSymbol: String,coinSymbol: String): BigDecimal {
        return sysConfigRepository.findByName(thirdCoinSymbol.toUpperCase()+'_'+ coinSymbol.toUpperCase() +'_'+"RATE").value.toBigDecimal()
    }

    @Autowired
    lateinit var sysConfigRepository:SysConfigRepository
}