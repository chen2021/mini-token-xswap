package io.nchain.token.xswap.domain.vo

import io.swagger.annotations.ApiModelProperty

class DepositAddrVO {
    @ApiModelProperty("充值地址")
    var addr: String? = null

    @ApiModelProperty("兑换汇率")
    var rate: String? = null

    @ApiModelProperty("反向:转账memo")
    var depositMemo: String? = null
}