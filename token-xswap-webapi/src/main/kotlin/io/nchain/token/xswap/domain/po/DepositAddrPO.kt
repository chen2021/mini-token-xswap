package io.nchain.token.xswap.domain.po

import io.nchain.token.xswap.valid.ListValue
import io.swagger.annotations.ApiModelProperty
import javax.validation.constraints.NotBlank

open class DepositAddrPO : PageablePO() {
    @ApiModelProperty("mgp钱包地址")
    @NotBlank(message = "[param error] walletAddr is null")
    var walletAddr: String? = null

    @ApiModelProperty("第三方主链名 eth、trc")
    @NotBlank(message = "[param error] thirdChain is null")
    var thirdChain: String? = null

    @ApiModelProperty("三方币名 usdt")
    @NotBlank(message = "[param error] thirdCoinSymbol is null")
    var thirdCoinSymbol: String? = null

    @ApiModelProperty("0:正向（erc 、trc-->mgp);1:反之")
    @ListValue(values = [0, 1])
    var direction: Int? = null

    @ApiModelProperty("我方链名 mgp")
    @NotBlank(message = "[param error] ownChain is null")
    var ownChain: String? = null

    @ApiModelProperty("我方币名 usdt")
    @NotBlank(message = "[param error] ownCoinSymbol is null")
    var ownCoinSymbol: String? = null

}