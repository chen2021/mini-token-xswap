package io.nchain.token.xswap.service

import io.nchain.token.xswap.domain.entity.DepositAddr
import io.nchain.token.xswap.domain.entity.ThirdCallbackLog
import io.nchain.token.xswap.domain.po.DepositCallbackPO
import io.nchain.token.xswap.domain.po.WithdrawCallbackPO

/**
 * @author     ：CHEN
 * @date       ：Created in 2020/11/14 14:55
 * @description：
 * @modified By：
 * @version    ：1.0
 */
interface CallBackService {
    fun saveDepositRecord(po: DepositCallbackPO): Pair<ThirdCallbackLog, DepositAddr>

    fun validateDeposit(po: DepositCallbackPO): Boolean

    fun validateWithdraw(po: WithdrawCallbackPO): Boolean
}