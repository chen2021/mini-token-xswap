package io.nchain.token.xswap.domain.po

import io.nchain.token.xswap.valid.ListValue
import io.swagger.annotations.ApiModelProperty

/**
 *
 * @author     : CHEN
 * @Date       : Created in 2021/3/14 1:53 上午
 * @description：
 * @modified By：
 * @Version    : 1.0
 **/
class SymbolPairPO {
    @ApiModelProperty("0:正向（erc 、trc-->mgp);1:反之")
    @ListValue(values = [0, 1])
    var direction: Int? = null
}