package io.nchain.token.xswap.domain.po

import io.swagger.annotations.ApiModelProperty
import javax.validation.constraints.NotBlank

/**
 *
 * @author     : CHEN
 * @Date       : Created in 2021/3/14 1:10 下午
 * @description：
 * @modified By：
 * @Version    : 1.0
 **/
class QuickSwapPO : DepositAddrPO() {

    @ApiModelProperty("三方链(trx、eth)地址")
    @NotBlank(message = "[param error] thirdAddr is null")
    var thirdAddr: String? = null

    @ApiModelProperty("充值备注")
    @NotBlank(message = "[param error] depositMemo is null")
    var depositMemo: String? = null
}