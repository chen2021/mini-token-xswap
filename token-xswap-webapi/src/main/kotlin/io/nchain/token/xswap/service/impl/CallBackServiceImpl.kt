package io.nchain.token.xswap.service.impl

import io.nchain.token.xswap.config.SdkConfigManager
import io.nchain.token.xswap.dict.DepositDirectionDict
import io.nchain.token.xswap.dict.DepositStatusType
import io.nchain.token.xswap.domain.entity.*
import io.nchain.token.xswap.domain.po.DepositCallbackPO
import io.nchain.token.xswap.domain.po.WithdrawCallbackPO
import io.nchain.token.xswap.domain.repository.*
import io.nchain.token.xswap.service.CallBackService
import io.nchain.token.xswap.service.RecordService
import io.nchain.token.xswap.utils.GenerateRateNameUtils
import io.nchain.token.xswap.utils.Md5
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.math.BigDecimal

/**
 * @author     ：CHEN
 * @date       ：Created in 2020/11/14 14:56
 * @description：
 * @modified By：
 * @version    ：1.0
 */
@Service
open class CallBackServiceImpl : CallBackService {

    override fun saveDepositRecord(po: DepositCallbackPO): Pair<ThirdCallbackLog, DepositAddr> {
        val result = thirdCallbackLogRepository.findOne(QThirdCallbackLog.qThirdCallbackLog.depositOrderId.eq(po.data.id))
        val record = if (result.isPresent) result.get() else ThirdCallbackLog()
        var direction = DepositDirectionDict.DIRECT.code
        if (po.data.subuserid!!.length > 18) direction = DepositDirectionDict.INDIRECT.code
        val depositAddr = if (direction == DepositDirectionDict.DIRECT.code)
            depositAddrRepository.findOne(QDepositAddr.qDepositAddr.thirdAddr.eq(po.data.addr)
                    .and(QDepositAddr.qDepositAddr.thirdCoinSymbol.eq(po.data.coin))).get()
        else depositAddrRepository.findOne(QDepositAddr.qDepositAddr.walletAddr.eq(po.data.from_addr)
                .and(QDepositAddr.qDepositAddr.depositMemo.eq(po.data.subuserid))).get()

        if (record.depositStatus != DepositStatusType.NORMAL_STATUS.code) {
            val rateName = if (direction == DepositDirectionDict.DIRECT.code)
                GenerateRateNameUtils.generateRateNameUtils(depositAddr.thirdChain, depositAddr.thirdCoinSymbol, depositAddr.ownChain, depositAddr.ownCoinSymbol)
            else
                GenerateRateNameUtils.generateRateNameUtils(depositAddr.ownChain, depositAddr.ownCoinSymbol, depositAddr.thirdChain, depositAddr.thirdCoinSymbol)
            val exchangeRate = exchangeRateRepository.findByName(rateName).exchageRate

            record.direction = direction
            record.exchangeRate = exchangeRate
            record.gas = if (direction == DepositDirectionDict.DIRECT.code) 0 else chainConfigRepository.findOne(QChainConfig.qChainConfig.chain.eq(depositAddr.thirdChain)).get().gas
            record.walletAddr = depositAddr.walletAddr
            record.depositAmount = po.data.amount?.toBigDecimal() ?: BigDecimal.ONE
            record.depositBalance = po.data.balance?.toBigDecimal() ?: BigDecimal.ONE
            record.depositOrderId = po.data.id!!
            record.depositTxid = po.data.txid
            record.depositChain = po.data.chain
            record.depositCoinSymbol = po.data.coin
            record.depositFromAddr = po.data.from_addr!!
            record.depositThirdAddr = po.data.addr
            record.depositConfirmedAt = po.data.time
            record.depositMemo = if (direction == DepositDirectionDict.DIRECT.code) null else depositAddr.depositMemo
            record.withdrawOrderId = -1 //初始化提币订单号
            record.withdrawChain = if (direction == DepositDirectionDict.DIRECT.code) depositAddr.ownChain else depositAddr.thirdChain
            record.withdrawToAddr = if (direction == DepositDirectionDict.DIRECT.code) depositAddr.walletAddr else depositAddr.thirdAddr
            record.withdrawCoinSymbol = if (direction == DepositDirectionDict.DIRECT.code) depositAddr.ownCoinSymbol else depositAddr.thirdCoinSymbol
            record.depositStatus = po.data.status!!.toInt()
            recordService.save(record)
        }
        return Pair(record, depositAddr)
    }

    override fun validateDeposit(po: DepositCallbackPO): Boolean {
        val sdkInfo = sdkConfigManager.getSdkInfo()
        val tokenStr = sdkInfo.apiKey + '_' + sdkInfo.secretKey + '_' + sdkInfo.userId + '_' + po.data.subuserid + '_' + po.data.auth.timestamp +
                '_' + po.data.chain + '_' + po.data.coin + '_' + po.data.addr + '_' + po.data.amount
        return Md5.md5(tokenStr) == po.data.auth.token
    }

    override fun validateWithdraw(po: WithdrawCallbackPO): Boolean {
        val sdkInfo = sdkConfigManager.getSdkInfo()
        val tokenStr = sdkInfo.apiKey + '_' + sdkInfo.secretKey + '_' + sdkInfo.userId + '_' + po.data.subuserid + '_' + po.data.auth.timestamp +
                '_' + po.data.chain + '_' + po.data.coin + '_' + po.data.addr + '_' + po.data.amount + '_' + po.data.status +
                '_' + po.data.memo + '_' + po.data.usertags
        return Md5.md5(tokenStr) == po.data.auth.token
    }

    @Autowired
    lateinit var chainConfigRepository: ChainConfigRepository

    @Autowired
    lateinit var depositAddrRepository: DepositAddrRepository

    @Autowired
    lateinit var recordService: RecordService

    @Autowired
    lateinit var exchangeRateRepository: ExchangeRateRepository

    @Autowired
    lateinit var thirdCallbackLogRepository: ThirdCallbackLogRepository

    @Autowired
    lateinit var sdkConfigManager: SdkConfigManager
}