package io.nchain.token.xswap.domain.vo

/**
 * @author     ：CHEN
 * @date       ：Created in 2020/11/16 0:18
 * @description：
 * @modified By：
 * @version    ：1.0
 */
class OkCallbackVO {
    var ok: Int = 1
    var msg: String = ""
}