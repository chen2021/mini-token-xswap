package io.nchain.token.xswap.controller

import com.dingtalk.api.DefaultDingTalkClient
import com.dingtalk.api.DingTalkClient
import com.dingtalk.api.request.OapiRobotSendRequest
import com.waykichain.securitiesaccessories.commons.NoPackBiz
import io.nchain.token.xswap.dict.DepositDirectionDict
import io.nchain.token.xswap.dict.ErrorCode
import io.nchain.token.xswap.dict.SysConfigName
import io.nchain.token.xswap.dict.WithdrawStatusType
import io.nchain.token.xswap.domain.entity.QThirdCallbackLog
import io.nchain.token.xswap.domain.po.DepositCallbackPO
import io.nchain.token.xswap.domain.po.WithdrawCallbackPO
import io.nchain.token.xswap.domain.repository.ThirdCallbackLogRepository
import io.nchain.token.xswap.domain.repository.SysConfigRepository
import io.nchain.token.xswap.domain.vo.CallBackResponse
import io.nchain.token.xswap.domain.vo.ErrorCallBackVO
import io.nchain.token.xswap.domain.vo.OkCallbackVO
import io.nchain.token.xswap.service.CallBackService
import io.nchain.token.xswap.service.SdkService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import pro.safeworld.swasdk.data.Req.ReqQueryBalanceBodyInfo
import pro.safeworld.swasdk.data.Req.ReqQueryWithdrawStatus
import java.math.BigDecimal

/**
 *
 * @author     : CHEN
 * @Date       : Created in 2021/3/14 3:24 上午
 * @description：
 * @modified By：
 * @Version    : 1.0
 **/
@RestController
@RequestMapping("callback")
class CallBackController {
    @PostMapping("deposit")
    @NoPackBiz
    fun deposit(@RequestBody po: DepositCallbackPO): CallBackResponse<*> {
        if (callBackService.validateDeposit(po)) {
            log.info("【充值回调】---订单id:${po.data.id}---交易ID:${po.data.txid}---发送方地址：${po.data.from_addr}---接收方地址：${po.data.addr}---充值数量:${po.data.amount}---充值状态：${po.data.status_desc}---子账户:${po.data.subuserid}")
            val vo = OkCallbackVO()
            if (!po.data.txid.isNullOrBlank()) {
                //存充值记录
                val res = callBackService.saveDepositRecord(po)
                val record = res.first
                val depositAddr = res.second
                if (record.direction == DepositDirectionDict.DIRECT.code) depositDingMsg(po.data.amount!!.toBigDecimal(), po.data.coin!!, po.data.id!!, po.data.from_addr!!, po.data.addr!!, po.data.txid!!, record.walletAddr, po.data.status_desc!!)
                else depositDingMsg(po.data.amount!!.toBigDecimal(), po.data.coin!!, po.data.id!!, po.data.from_addr!!, po.data.addr!!, po.data.txid!!, depositAddr.thirdAddr, po.data.status_desc!!)
            } else {
                val v = ErrorCallBackVO()
                v.eno = 1
                v.emsg = ErrorCode.SYSYTEM_ERROR.msg

                return CallBackResponse(v)
            }
            return CallBackResponse(vo)
        } else {
            log.error("【充值回调】---第三方身份有误")
            val v = ErrorCallBackVO()
            v.eno = ErrorCode.TOKEN_VERIFIY_FAILED.code
            v.emsg = ErrorCode.TOKEN_VERIFIY_FAILED.msg
            return CallBackResponse(v)
        }
    }

    @PostMapping("withdraw")
    @NoPackBiz
    fun withdraw(@RequestBody po: WithdrawCallbackPO): CallBackResponse<*> {
        if (callBackService.validateWithdraw(po)) {
            log.info("【提币回调】---内部充值id:${po.data.id}---链上交易ID:${po.data.txid}---提币发送地址：${po.data.from_addr}---提币接收地址：${po.data.addr}---提币数量:${po.data.amount}---实际提币数量:${po.data.amount_sent}" +
                    "---提币哈希：${po.data.txid}---提币状态：${po.data.status_desc}")
            val reqQueryWithdrawStatus = ReqQueryWithdrawStatus()
            reqQueryWithdrawStatus.chain = po.data.chain!!
            reqQueryWithdrawStatus.coin = po.data.coin!!
            reqQueryWithdrawStatus.withdrawid = po.data.id!!.toLong()
            val result = sdkService.queryWithdrawStatus(reqQueryWithdrawStatus)
            return if (result.eno == 0) {
                val okCallbackVO = OkCallbackVO()
                val record = thirdCallbackLogRepository.findOne(QThirdCallbackLog.qThirdCallbackLog.withdrawOrderId.eq(po.data.id!!.toLong())).get()
                if (record.withdrawStatus != WithdrawStatusType.SEND_SUCCESSED.code) {
                    record.withdrawAmount = po.data.amount?.toBigDecimal() ?: BigDecimal.ONE
                    record.withdrawAmountSent = po.data.amount_sent?.toBigDecimal() ?: BigDecimal.ONE
                    record.withdrawStatus = if (po.data.status == WithdrawStatusType.SEND_SUCCESSED.code) WithdrawStatusType.SEND_SUCCESSED.code else po.data.status!!
                    record.statusDesc = if (po.data.status == WithdrawStatusType.SEND_SUCCESSED.code) WithdrawStatusType.SEND_SUCCESSED.msg else po.data.status_desc
                    record.withdrawTxid = po.data.txid
                    record.withdrawFromAddr = po.data.from_addr
                    thirdCallbackLogRepository.update(record.withdrawAmount, record.withdrawAmountSent, record.withdrawStatus, record.statusDesc, record.withdrawTxid, record.withdrawFromAddr, record.withdrawOrderId)
                    //查询第三方打币地址余额
                    var coinBalance: String
                    try {
                        val coinSymbol = record.withdrawCoinSymbol.toLowerCase()
                        val set = HashSet<ReqQueryBalanceBodyInfo>()
                        val reqQueryBalanceBodyInfo = ReqQueryBalanceBodyInfo()
                        reqQueryBalanceBodyInfo.chain = record.withdrawChain
                        reqQueryBalanceBodyInfo.coin = record.withdrawCoinSymbol
                        set.add(reqQueryBalanceBodyInfo)
                        val balance = sdkService.queryBalance(set)
                        coinBalance = balance.firstOrNull {
                            it.coin == coinSymbol
                        }.let {
                            it?.balance ?: ""
                        }
                    } catch (e: Exception) {
                        coinBalance = "查询余额有误"
                    }
                    if (record.direction == DepositDirectionDict.DIRECT.code) withdrawDingMsg(record.depositAmount, record.withdrawAmount, record.withdrawCoinSymbol, record.withdrawAmountSent, record.withdrawOrderId, record.withdrawFromAddr, record.walletAddr, record.withdrawTxid, po.data.status_desc!!, coinBalance)
                    else withdrawDingMsg(record.depositAmount, record.withdrawAmount, record.withdrawCoinSymbol, record.withdrawAmountSent, record.withdrawOrderId, record.withdrawFromAddr, record.withdrawToAddr, record.withdrawTxid, po.data.status_desc!!, coinBalance)
                }
                return CallBackResponse(okCallbackVO)

            } else {
                val errorCallBackVO = ErrorCallBackVO()
                errorCallBackVO.eno = result.eno
                errorCallBackVO.emsg = result.emsg ?: ""
                CallBackResponse(errorCallBackVO)
            }
        } else {
            log.error("【提币回调】---第三方身份有误")
            val v = ErrorCallBackVO()
            v.eno = ErrorCode.TOKEN_VERIFIY_FAILED.code
            v.emsg = ErrorCode.TOKEN_VERIFIY_FAILED.msg
            return CallBackResponse(v)
        }
    }

    private fun depositDingMsg(amount: BigDecimal, coin: String, depositId: Int, fromAddr: String, addr: String, depositTxId: String, walletAddr: String, status_desc: String) {
        val url = sysConfigRepository.findByName(SysConfigName.DING_DEPOSIT_URL.cfgName).value
        val client: DingTalkClient = DefaultDingTalkClient(url)
        val request = OapiRobotSendRequest()

        request.msgtype = "text"
        val text = OapiRobotSendRequest.Text()
        text.content = "【您有一笔新的充币记录】\n" +
                "数量：${amount}\n" +
                "币种：${coin}\n" +
                "后台订单ID：${depositId}\n" +
                "打币地址：${fromAddr}\n" +
                "收款地址：${addr}\n" +
                "交易哈希：${depositTxId}\n" +
                "关联用户地址：${walletAddr}\n" +
                "充值状态：${status_desc}\n"

        request.setText(text)
        try {
            client.execute(request)
        } catch (e: Exception) {

        }
    }

    private fun withdrawDingMsg(depositAmount: BigDecimal, withdrawAmount: BigDecimal, coin: String, amountSent: BigDecimal, depositId: Long, fromAddr: String, addr: String, withdrawTxId: String, status_desc: String, coinBalance: String) {
        val url = sysConfigRepository.findByName(SysConfigName.DING_WITHDRAW_URL.cfgName).value
        val client: DingTalkClient = DefaultDingTalkClient(url)
        val request = OapiRobotSendRequest()

        request.msgtype = "text"
        val text = OapiRobotSendRequest.Text()
        text.content = "【您有一笔新的提币记录】\n" +
                "充币数量：${depositAmount}\n" +
                "申请提币数量：${withdrawAmount}\n" +
                "实际提币数量：${amountSent}\n" +
                "币种：${coin}\n" +
                "打币地址余额（${coin}）：${coinBalance}\n" +
                "后台订单ID：${depositId}\n" +
                "打币地址：${fromAddr}\n" +
                "收款地址：${addr}\n" +
                "交易哈希：${withdrawTxId}\n" +
                "提币状态：${status_desc}"


        request.setText(text)
        try {
            client.execute(request)
        } catch (e: Exception) {

        }
    }


    private val log = LoggerFactory.getLogger(javaClass)

    @Autowired
    lateinit var sdkService: SdkService

    @Autowired
    lateinit var callBackService: CallBackService

    @Autowired
    lateinit var sysConfigRepository: SysConfigRepository

    @Autowired
    lateinit var thirdCallbackLogRepository: ThirdCallbackLogRepository
}