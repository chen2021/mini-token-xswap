package io.nchain.token.xswap.service.impl

import io.nchain.token.xswap.dict.DepositDirectionDict
import io.nchain.token.xswap.domain.entity.QSwapConfig
import io.nchain.token.xswap.domain.po.SymbolPairPO
import io.nchain.token.xswap.domain.repository.SwapConfigRepository
import io.nchain.token.xswap.domain.vo.SymbolPairVO
import io.nchain.token.xswap.domain.vo.SymbolPairs
import io.nchain.token.xswap.service.SwapService
import io.nchain.token.xswap.utils.GenerateRateNameUtils
import org.springframework.beans.BeanUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.annotation.PostConstruct

/**
 *
 * @author     : CHEN
 * @Date       : Created in 2021/3/17 11:12 上午
 * @description：
 * @modified By：
 * @Version    : 1.0
 **/
@Service
class SwapServiceImpl : SwapService {
    private var map = HashMap<String, Pair<String, String>>()

    @PostConstruct
    fun init() {
        swapConfigRepository.findAll(QSwapConfig.qSwapConfig.swapinOn.eq(1)).forEach {
            val flag = DepositDirectionDict.DIRECT.code.toString() + ":" + GenerateRateNameUtils.generateRateNameUtils(it.thirdChain, it.thirdCoinSymbol, it.ownChain, it.ownCoinSymbol)
            map[flag] = Pair(it.thirdCoinSymbolDisplay, it.ownCoinSymbolDisplay)
        }
        swapConfigRepository.findAll(QSwapConfig.qSwapConfig.swapoutOn.eq(1)).forEach {
            val flag = DepositDirectionDict.INDIRECT.code.toString() + ":" + GenerateRateNameUtils.generateRateNameUtils(it.ownChain, it.ownCoinSymbol, it.thirdChain, it.thirdCoinSymbol)
            map[flag] = Pair(it.ownCoinSymbolDisplay, it.thirdCoinSymbolDisplay)
        }
    }

    override fun getPairCoinSymbol(flag: String): Pair<String, String> {
        return map[flag]!!
    }

    override fun refreshSwapConfig() {
        map.clear()
        init()
    }

    override fun getSymbolPair(po: SymbolPairPO): SymbolPairVO {
        val vo = SymbolPairVO()
        if (po.direction == DepositDirectionDict.DIRECT.code) {
            swapConfigRepository.findAll(QSwapConfig.qSwapConfig.swapinOn.eq(1)).forEach {
                val res = SymbolPairs()
                BeanUtils.copyProperties(it, res)
                res.direction = po.direction!!
                res.sortAt = it.displayOrder
                res.depositCoinSymbol = it.thirdCoinSymbolDisplay
                res.depositLogo = it.thirdCoinLogo
                res.withdrawCoinSymbol = it.ownCoinSymbolDisplay
                res.withdrawLogo = it.ownCoinLogo
                vo.symbolPairList.add(res)
            }
        } else {
            swapConfigRepository.findAll(QSwapConfig.qSwapConfig.swapoutOn.eq(1)).forEach {
                val res = SymbolPairs()
                BeanUtils.copyProperties(it, res)
                res.direction = po.direction!!
                res.sortAt = it.displayOrder
                res.depositCoinSymbol = it.ownCoinSymbolDisplay
                res.depositLogo = it.ownCoinLogo
                res.withdrawCoinSymbol = it.thirdCoinSymbolDisplay
                res.withdrawLogo = it.thirdCoinLogo
                vo.symbolPairList.add(res)
            }
        }
        vo.symbolPairList.sortBy { it.sortAt }

        return vo
    }

    @Autowired
    lateinit var swapConfigRepository: SwapConfigRepository
}