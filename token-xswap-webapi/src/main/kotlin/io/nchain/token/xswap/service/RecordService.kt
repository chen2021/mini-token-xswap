package io.nchain.token.xswap.service


import io.nchain.token.xswap.domain.entity.ThirdCallbackLog
import io.nchain.token.xswap.domain.po.DepositAddrPO
import io.nchain.token.xswap.domain.po.RecordDetailPO
import io.nchain.token.xswap.domain.vo.RecordDetailVO
import io.nchain.token.xswap.domain.vo.RecordVO

/**
 * @author     ：CHEN
 * @date       ：Created in 2020/11/16 10:46
 * @description：
 * @modified By：
 * @version    ：1.0
 */
interface RecordService {
    fun save(thirdCallbackLog: ThirdCallbackLog)

    fun getList(po: DepositAddrPO): RecordVO

    fun getDetail(po: RecordDetailPO): RecordDetailVO
}