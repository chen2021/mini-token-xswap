package io.nchain.token.xswap.domain.po

import com.fasterxml.jackson.annotation.JsonFormat
import io.nchain.token.xswap.domain.bean.DepositAddr

import java.util.*


class DepositCallbackPO {
    var appid: String? = null
    var cryptype: Int = 0
    var data = CallbackPOData()

    class CallbackPOData {
        var auth = DepositAddr.DepositData.Auth()
        var id: Int? = null
        var subuserid: String? = null
        var chain: String? = null
        var coin: String? = null
        var from_addr: String? = null
        var addr: String? = null
        var txid: String? = null
        var amount: String? = null
        var balance: String? = null
        var height: String? = null
        var status: String? = null
        var status_desc: String? = null

        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
        var time: Date? = null
    }
}