package io.nchain.token.xswap.domain.vo

import io.swagger.annotations.ApiModelProperty
import javax.validation.constraints.NotBlank

/**
 * @author     ：CHEN
 * @date       ：Created in 2021/1/15 9:56
 * @description：
 * @modified By：
 * @version    ：1.0
 */
class SymbolPairVO {
    var symbolPairList = arrayListOf<SymbolPairs>()
}

class SymbolPairs {

    @ApiModelProperty("第三方主链名（小写）")
    var thirdChain: String = "eth"

    @ApiModelProperty("三方币名（小写）")
    var thirdCoinSymbol: String = "usdt"

    @ApiModelProperty("0:正向（erc 、trc-->mgp);1:反之")
    var direction: Int = 0

    @ApiModelProperty("我方链名 MGP")
    var ownChain:String?=null

    @ApiModelProperty("我方币名 USDT")
    var ownCoinSymbol:String?=null

    @ApiModelProperty("充币类型")
    var depositCoinSymbol:String ="ERC20_USDT"

    @ApiModelProperty("提币类型 ")
    var withdrawCoinSymbol: String? = null

    @ApiModelProperty("排序")
    var sortAt: Int = 0

    @ApiModelProperty("logo")
    var depositLogo: String? = null

    @ApiModelProperty("logo")
    var withdrawLogo: String? = null

}