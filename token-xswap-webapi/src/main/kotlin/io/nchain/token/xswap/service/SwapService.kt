package io.nchain.token.xswap.service

import io.nchain.token.xswap.domain.po.SymbolPairPO
import io.nchain.token.xswap.domain.vo.SymbolPairVO

/**
 *
 * @author     : CHEN
 * @Date       : Created in 2021/3/17 11:11 上午
 * @description：
 * @modified By：
 * @Version    : 1.0
 **/
interface SwapService {
    fun getSymbolPair(po: SymbolPairPO): SymbolPairVO

    fun getPairCoinSymbol(flag: String): Pair<String, String>

    fun refreshSwapConfig()
}