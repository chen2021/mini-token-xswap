package io.nchain.token.xswap.controller

import com.waykichain.securitiesaccessories.commons.BizException
import com.waykichain.securitiesaccessories.commons.BizResponse
import io.nchain.token.xswap.config.SdkConfigManager
import io.nchain.token.xswap.dict.ErrorCode
import io.nchain.token.xswap.domain.entity.SysConfig
import io.nchain.token.xswap.domain.repository.SysConfigRepository
import io.nchain.token.xswap.service.SdkService
import io.nchain.token.xswap.service.SwapService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*

/**
 *
 * @author     : CHEN
 * @Date       : Created in 2021/3/12 12:48 上午
 * @description：
 * @modified By：
 * @Version    : 1.0
 **/
@Api("ops")
@RestController
@RequestMapping("ops")
class OpsController {
    @GetMapping("ver")
    @ApiOperation("ops", httpMethod = "GET", notes = "ops", produces = MediaType.APPLICATION_JSON_VALUE)
    fun test(): BizResponse<Any> {
        return BizResponse("it's ok")
    }

    private val log = LoggerFactory.getLogger(OpsController::class.java)

    @Autowired
    lateinit var sysConfigRepository: SysConfigRepository

    @Autowired
    lateinit var sdkService: SdkService

}