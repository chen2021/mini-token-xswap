package io.nchain.token.xswap.controller

import io.nchain.token.xswap.domain.po.SymbolPairPO
import io.nchain.token.xswap.domain.vo.SymbolPairVO
import io.nchain.token.xswap.service.SwapService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

/**
 *
 * @author     : CHEN
 * @Date       : Created in 2021/3/17 11:10 上午
 * @description：
 * @modified By：
 * @Version    : 1.0
 **/
@Api(description = "刚兑信息")
@RestController
@RequestMapping("swap")
class SwapController {
    @PostMapping("symbol_pair")
    @ApiOperation(value = "可兑换币种信息", notes = "可兑换币种信息", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    fun getSymbolPair(@Validated @RequestBody po: SymbolPairPO): SymbolPairVO {
        return swapService.getSymbolPair(po)

    }

    @Autowired
    lateinit var swapService: SwapService
}