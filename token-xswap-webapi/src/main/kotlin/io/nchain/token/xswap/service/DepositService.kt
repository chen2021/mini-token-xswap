package io.nchain.token.xswap.service

import io.nchain.token.xswap.domain.po.DepositAddrPO
import io.nchain.token.xswap.domain.po.SymbolPairPO
import io.nchain.token.xswap.domain.po.QuickSwapPO
import io.nchain.token.xswap.domain.vo.DepositAddrVO
import io.nchain.token.xswap.domain.vo.SymbolPairVO

/**
 *
 * @author     : CHEN
 * @Date       : Created in 2021/3/13 1:44 下午
 * @description：
 * @modified By：
 * @Version    : 1.0
 **/
interface DepositService {
    fun getDepositAddr(po: DepositAddrPO, ip: String?): DepositAddrVO

    fun quickSwap(po: QuickSwapPO,ip: String?)
}