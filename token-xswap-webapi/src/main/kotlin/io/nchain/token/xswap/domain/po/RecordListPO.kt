package io.nchain.token.xswap.domain.po

import io.swagger.annotations.ApiModelProperty
import javax.validation.constraints.Min

/**
 * @author     ：CHEN
 * @date       ：Created in 2020/11/16 10:16
 * @description：
 * @modified By：
 * @version    ：1.0
 */
class RecordListPO: BasePageablePO(){
    @ApiModelProperty("状态（订单状态 0-未提币 1-准备发送  2-发送中 3-发送成功 4-发送失败 5-发送已取消）")
    var status: Int? = null

    var id: Long =0
}
open class BasePageablePO {

    @ApiModelProperty(value = "当前页，从1开始（默认1）", example = "1")
    var currentpage: Int = 1

    @ApiModelProperty(value="每页条数（默认20）", example = "20")
    @Min(value=1,message = "[in param error] Minimum value of pageSize is 1")
    var pagesize: Int = 20

}