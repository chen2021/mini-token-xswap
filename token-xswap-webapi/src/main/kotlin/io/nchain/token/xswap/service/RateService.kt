package io.nchain.token.xswap.service

import java.math.BigDecimal

/**
 * @author     ：CHEN
 * @date       ：Created in 2020/11/16 11:19
 * @description：
 * @modified By：
 * @version    ：1.0
 */
interface RateService {
    fun queryRate(thirdCoinSymbol: String,coinSymbol: String): BigDecimal
}