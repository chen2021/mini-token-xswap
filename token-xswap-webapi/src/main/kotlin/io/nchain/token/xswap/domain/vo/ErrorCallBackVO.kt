package io.nchain.token.xswap.domain.vo

/**
 * @author     ：CHEN
 * @date       ：Created in 2020/11/16 0:26
 * @description：
 * @modified By：
 * @version    ：1.0
 */
class ErrorCallBackVO {

    var eno: Int =0
    var emsg: String = ""
    var data = DataSec()

    class DataSec {
        constructor()
    }

}