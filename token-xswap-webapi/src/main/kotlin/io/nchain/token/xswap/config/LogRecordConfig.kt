package io.nchain.token.xswap.config

import com.waykichain.securitiesaccessories.tracelog.HttpTraceData
import com.waykichain.securitiesaccessories.tracelog.HttpTraceLogFilter
import io.nchain.token.xswap.domain.entity.SysRequestLog
import io.nchain.token.xswap.domain.repository.SysRequestLogRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.DependsOn
import org.springframework.stereotype.Component
import java.util.*

/**
 * @author     ：CHEN
 * @date       ：Created in 2020/12/25 15:39
 * @description：
 * @modified By：
 * @version    ：1.0
 */
@Component
@DependsOn("entityManagerFactory")
class LogRecordConfig : HttpTraceLogFilter() {

    override fun obtainNotAllowContainUri(): MutableList<String> {
        return mutableListOf()
    }

    override fun obtainNotAllowContentType(): MutableList<String> {
        return mutableListOf()
    }

    override fun dealLog(httpTraceData: HttpTraceData?) {
        val ip = httpTraceData!!.ip
        val method = httpTraceData.method
        val uri = httpTraceData.uri
        val params = httpTraceData.params
        val requestBoy = httpTraceData.requestBoy
        val responseBoy = httpTraceData.responseBoy

        val logRecord = SysRequestLog()
        logRecord.ip = ip
        logRecord.method = method
        logRecord.uri = uri
        logRecord.params = params
        logRecord.requestBody = requestBoy
        logRecord.responseBody = responseBoy
        logRecord.createdAt = Date()
        sysRequestLogRepository.save(logRecord)
        //将ip转int类型保存
      /*  val intIp= IpUtil.ipToInt(ip)
        val ipAd=IpUtil.intToIp(intIp)
        logger.info("${intIp}------${ipAd}")*/
    }

    @Autowired
    lateinit var sysRequestLogRepository:SysRequestLogRepository

}