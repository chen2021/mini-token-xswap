package io.nchain.token.xswap.controller

import com.waykichain.securitiesaccessories.commons.BizResponse
import io.nchain.token.xswap.domain.po.DepositAddrPO
import io.nchain.token.xswap.domain.po.QuickSwapPO
import io.nchain.token.xswap.domain.vo.DepositAddrVO
import io.nchain.token.xswap.service.DepositService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*

/**
 *
 * @author     : CHEN
 * @Date       : Created in 2021/3/13 1:43 下午
 * @description：
 * @modified By：
 * @Version    : 1.0
 **/

@Api(description = "充值相关")
@RestController
@RequestMapping("deposit")
class DepositController : BaseController() {
    @PostMapping("get_addr")
    @ApiOperation(value = "获取充值地址", httpMethod = "POST", notes = "获取充值地址", produces = MediaType.APPLICATION_JSON_VALUE)
    fun getDepositAddr(@Validated @RequestBody po: DepositAddrPO): DepositAddrVO {
        val ip = getIpAddr(request)
        return depositService.getDepositAddr(po, ip)
    }

    @PostMapping("quick_swap")
    @ApiOperation(value = "立即兑换", httpMethod = "POST", notes = "立即兑换", produces = MediaType.APPLICATION_JSON_VALUE)
    fun quickSwap(@RequestBody po: QuickSwapPO): BizResponse<Any> {
        val ip = getIpAddr(request)
        depositService.quickSwap(po, ip)
        return BizResponse("")
    }

    @Autowired
    lateinit var depositService: DepositService
}