package io.nchain.token.xswap.domain.vo

/**
 * @author     ：CHEN
 * @date       ：Created in 2020/12/29 10:33
 * @description：
 * @modified By：
 * @version    ：1.0
 */
class IpVO {
    var ret:String?=null
    var ip:String?=null
    var data = ArrayList<String>()
}