package io.nchain.token.xswap.config

import com.waykichain.mysqlbase.dataconfig.DataSourceClient
import com.waykichain.mysqlbase.dataconfig.DatasourceConfig

/**
 *
 * @author Sugar
 * Created by 2020/11/12
 *
 **/
@DataSourceClient(basePackages = ["io.nchain.token.xswap.domain"])
class TokenXswapData: DatasourceConfig()