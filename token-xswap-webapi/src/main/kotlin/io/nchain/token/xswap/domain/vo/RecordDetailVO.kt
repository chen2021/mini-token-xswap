package io.nchain.token.xswap.domain.vo

import io.swagger.annotations.ApiModelProperty
import java.math.BigDecimal

/**
 *
 * @author     : CHEN
 * @Date       : Created in 2021/3/17 6:27 下午
 * @description：
 * @modified By：
 * @Version    : 1.0
 **/
class RecordDetailVO {
    @ApiModelProperty("数据库id")
    var id: Long? = null

    @ApiModelProperty("钱包地址")
    var walletAddr: String? = null

    @ApiModelProperty("充币类型")
    var depositCoinSymbol:String ="ERC20_USDT"

    @ApiModelProperty("提币类型 ")
    var withdrawCoinSymbol: String? = null

    @ApiModelProperty("状态：兑换成功 、待确认")
    var statusDesc: String? = null

    @ApiModelProperty("充币发送地址")
    var depositFromAddr: String? = null

    @ApiModelProperty("提币目标地址")
    var withdrawToAddr: String? = null

    @ApiModelProperty("充币数量")
    var depositAmount: BigDecimal? = null

    @ApiModelProperty("提币数量")
    var withdrawAmountSent: BigDecimal? = null

    @ApiModelProperty("充币交易哈希")
    var depositTxId: String? = null

    @ApiModelProperty("提币交易哈希")
    var withdrawTxId: String? = null

    @ApiModelProperty("充币确认时间")
    var depositConfirmTime: Long? = null

    @ApiModelProperty("提币确认时间")
    var withdrawConfirmTime: Long? = null
}