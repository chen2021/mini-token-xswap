package io.nchain.token.xswap

import com.waykichain.securitiesaccessories.utils.EnvironmentInit
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cache.annotation.EnableCaching
import springfox.documentation.oas.annotations.EnableOpenApi



/**
 * @author     ：CHEN
 * @date       ：Created in 2021/1/24 14:28
 * @description：
 * @modified By：
 * @version    ：1.0
 */
@SpringBootApplication
@EnableCaching
@EnableOpenApi
open class TokenXSwapApplication

fun main(args: Array<String>) {
    EnvironmentInit.initTimeZone(0)
    runApplication<TokenXSwapApplication>(*args)
}