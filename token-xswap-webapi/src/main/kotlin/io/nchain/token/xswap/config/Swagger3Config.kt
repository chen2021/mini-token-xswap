package io.nchain.token.xswap.config

import io.swagger.annotations.ApiOperation
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import springfox.documentation.builders.*
import springfox.documentation.oas.annotations.EnableOpenApi
import springfox.documentation.service.*
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import java.util.*

/**
 *
 * @author     : CHEN
 * @Date       : Created in 2021/3/18 1:33 上午
 * @description：
 * @modified By：
 * @Version    : 1.0
 **/
@EnableOpenApi
@Configuration
open class Swagger3Config : WebMvcConfigurer {
    @Bean
    open fun createRestApi(): Docket {
        //返回文档摘要信息
        return Docket(DocumentationType.OAS_30)
                .apiInfo(apiInfo())
                .select() //.apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation::class.java))
                .paths(PathSelectors.any())
                .build()
                .globalResponses(HttpMethod.GET, getGlobalResonseMessage())
                .globalResponses(HttpMethod.POST, getGlobalResonseMessage())
    }

    //生成接口信息，包括标题、联系人等
    private fun apiInfo(): ApiInfo {
        return ApiInfoBuilder()
                .title("mini-token-xswap 接口文档")
                .description("如有疑问，请联系开发工程师CHEN。")
                .contact(Contact("CHEN", "", "1685095924@qq.com"))
                .version("1.0")
                .build()
    }
    //生成通用响应信息
    private fun getGlobalResonseMessage(): List<Response> {
        val responseList: MutableList<Response> = ArrayList<Response>()
        responseList.add(ResponseBuilder().code("404").description("找不到资源").build())
        return responseList
    }
}